package com.ksp.eservices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;
import com.ksp.eservices.model.Categories;
import com.ksp.eservices.model.getCustomerDashboardData.DASH_QUT_DATA;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {
    Context context;
    List<DASH_QUT_DATA> categories;

    public CategoryAdapter(Context context, List<DASH_QUT_DATA> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoryAdapter.CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotationed1,parent,false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.CategoryHolder holder, int position) {

        Glide.with(context).load(categories.get(position).getFilepath()).into(holder.dash_qut_img);
        holder.dash_qut_date.setText(categories.get(position).getReq_datetime());
        holder.dash_qut_prd_name.setText(categories.get(position).getPrd_name());
        holder.dash_retailer.setText(String.format("Retailers - %s", categories.get(position).getTotal_retailer_cnt()));
        holder.dash_price.setText(String.format("Best Price - %s", categories.get(position).getBest_price()));

    }

    @Override
    public int getItemCount() {
        return categories==null?0:categories.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        AppCompatImageView dash_qut_img;
        TextView dash_qut_date, dash_qut_prd_name, dash_retailer, dash_price;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            dash_qut_img = itemView.findViewById(R.id.dash_prd_img);
            dash_qut_date = itemView.findViewById(R.id.dash_qut_date);
            dash_qut_prd_name = itemView.findViewById(R.id.dash_qut_prd_name);
            dash_retailer = itemView.findViewById(R.id.dash_retailer);
            dash_price = itemView.findViewById(R.id.dash_price);
        }
    }
}
