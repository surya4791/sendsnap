package com.ksp.eservices.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.activity.AddAddress;
import com.ksp.eservices.model.getAllCustomerAddresses.ADDRESS_DATA;

import java.util.List;

public class AllCustomerAddressAdapter extends RecyclerView.Adapter<AllCustomerAddressAdapter.AllAddressHolder> {

    private Context context;
    private List<ADDRESS_DATA> address_dataList;
    private SharedPreferences preferences;

    public AllCustomerAddressAdapter(Context context, List<ADDRESS_DATA> address_dataList) {
        this.context = context;
        this.address_dataList = address_dataList;
    }

    @NonNull
    @Override
    public AllAddressHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_address_list_adapter,parent,false);
        return new AllAddressHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllAddressHolder holder, int position) {
        preferences = context.getSharedPreferences("SendSnap",Context.MODE_PRIVATE);
        holder.adrs_type.setText("Home");
        holder.owner_name.setText(address_dataList.get(position).getName());
        holder.adrs_details.setText(String.format("%s, %s, %s, %s - %s", address_dataList.get(position).getHouse_no(), address_dataList.get(position).getLand_mark(), address_dataList.get(position).getStreet(), address_dataList.get(position).getCity(), address_dataList.get(position).getPin_code()));
        holder.select_adrs.setOnCheckedChangeListener((buttonView, isChecked) -> {
            preferences.edit().putString("AddressTxnRefNo", address_dataList.get(position).getRef_no()).apply();
           // ((AddAddress) context).getTxnRefNo(address_dataList.get(position).getRef_no());
        });

    }

    @Override
    public int getItemCount() {
        return address_dataList==null?0:address_dataList.size();
    }

    public class AllAddressHolder extends RecyclerView.ViewHolder {

        MaterialTextView adrs_type ,owner_name, adrs_details;
        RadioButton select_adrs;

        public AllAddressHolder(@NonNull View itemView) {
            super(itemView);
            adrs_type = itemView.findViewById(R.id.adrs_type);
            owner_name = itemView.findViewById(R.id.owner_name);
            adrs_details = itemView.findViewById(R.id.adrs_details);
            select_adrs = itemView.findViewById(R.id.select_radio);
        }
    }
}
