package com.ksp.eservices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;

import java.util.List;

public class QuotationChooseItemAdapter extends RecyclerView.Adapter<QuotationChooseItemAdapter.ChooseItemHolder> {
    Context context;
    List<String> stringList;

    public QuotationChooseItemAdapter(Context context, List<String> stringList) {
        this.context = context;
        this.stringList = stringList;
    }

    @NonNull
    @Override
    public ChooseItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotation_choose_item_name,parent,false);
        return new ChooseItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseItemHolder holder, int position) {
        holder.choose_item_name.setText(stringList.get(position));
    }

    @Override
    public int getItemCount() {
        return stringList==null?0:stringList.size();
    }

    public class ChooseItemHolder extends RecyclerView.ViewHolder {

        Chip choose_item_name;

        public ChooseItemHolder(@NonNull View itemView) {
            super(itemView);
            choose_item_name = itemView.findViewById(R.id.choose_item_chip);
        }
    }
}
