package com.ksp.eservices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;
import com.ksp.eservices.model.ShoppingImageItem;

import java.util.List;

public class ShoppingImageAdapter extends RecyclerView.Adapter<ShoppingImageAdapter.ShoppingImageHolder> {
    Context context;
    List<ShoppingImageItem> shoppingImageItems;
    int index = -1;

    public ShoppingImageAdapter(Context context, List<ShoppingImageItem> shoppingImageItems) {
        this.context = context;
        this.shoppingImageItems = shoppingImageItems;
    }

    @NonNull
    @Override
    public ShoppingImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.find_your_image_item,parent,false);
        return new ShoppingImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingImageHolder holder, int position) {

      //  Glide.with(context).load(shoppingImageItems.get(position)).into(holder.shopping_icon);

        if (index == position) holder.shopping_icon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selected_image_background));
        else holder.shopping_icon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.img_background));

        holder.shopping_icon.setOnClickListener(v -> {

            int prepos = index;
            index = position;
            notifyItemChanged(prepos);
            notifyItemChanged(position);

        });

    }

    @Override
    public int getItemCount() {
        return shoppingImageItems==null?0:shoppingImageItems.size();
    }

    public class ShoppingImageHolder extends RecyclerView.ViewHolder {
        AppCompatImageView shopping_icon;

        public ShoppingImageHolder(@NonNull View itemView) {
            super(itemView);
            shopping_icon = itemView.findViewById(R.id.shopping_icon);
        }
    }
}
