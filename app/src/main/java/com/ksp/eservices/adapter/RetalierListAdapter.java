package com.ksp.eservices.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.google.android.material.card.MaterialCardView;
import com.ksp.eservices.R;
import com.ksp.eservices.activity.AddAddress;
import com.ksp.eservices.activity.PlaceOrder;
import com.ksp.eservices.model.getAllRetailerForQuat.RETAILER_DATA;

import java.util.List;

public class RetalierListAdapter extends RecyclerView.Adapter<RetalierListAdapter.RetailerHolder> {
    private Context context;
    private List<RETAILER_DATA> retailer_data;
    private int count = 0;
    private SharedPreferences preferences;

    public RetalierListAdapter(Context context, List<RETAILER_DATA> retailer_data) {
        this.context = context;
        this.retailer_data = retailer_data;
    }

    @NonNull
    @Override
    public RetailerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.retailer_list_adapter,parent,false);
        return new RetailerHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull RetailerHolder holder, int position) {

        preferences = context.getSharedPreferences("SendSnap",Context.MODE_PRIVATE);

        count++;
        holder.retailer_count.setText(String.format("Retailer - %d", count));
        holder.store_name.setText(retailer_data.get(position).getRetailer_name());
        holder.prd_price.setText(String.format("%s/-", retailer_data.get(position).getOffered_price()));
        holder.full_adrss.setText(retailer_data.get(position).getAddress_1());

        holder.retailer_card.setOnClickListener(v -> {
            if (holder.expand_card.getVisibility() == View.GONE) {
                TransitionManager.beginDelayedTransition(holder.retailer_card, new AutoTransition());
                holder.expand_card.setVisibility(View.VISIBLE);
                holder.arrow_down.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            } else {
                TransitionManager.beginDelayedTransition(holder.retailer_card, new AutoTransition());
                holder.expand_card.setVisibility(View.GONE);
                holder.arrow_down.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            }
        });

        holder.place_order.setOnClickListener(v -> {
            preferences.edit().putString("retailer_id",retailer_data.get(position).getRetailer_id()).apply();
            context.startActivity(new Intent(context, AddAddress.class));
        });

    }

    @Override
    public int getItemCount() {
        return retailer_data==null?0:retailer_data.size();
    }

    public class RetailerHolder extends RecyclerView.ViewHolder {
        TextView retailer_count, store_name, prd_price, full_adrss;
        MaterialCardView retailer_card;
        LinearLayout expand_card;
        AppCompatButton place_order;
        AppCompatImageView arrow_down;

        public RetailerHolder(@NonNull View itemView) {
            super(itemView);
            retailer_count = itemView.findViewById(R.id.retailer_count);
            retailer_card = itemView.findViewById(R.id.retailer_card);
            expand_card = itemView.findViewById(R.id.expand_card);
            place_order = itemView.findViewById(R.id.place_order1);
            arrow_down = itemView.findViewById(R.id.arrow_down);
            store_name = itemView.findViewById(R.id.store_title);
            prd_price = itemView.findViewById(R.id.rupees);
            full_adrss = itemView.findViewById(R.id.address_txt);
        }
    }
}
