package com.ksp.eservices.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;
import com.ksp.eservices.activity.GetQuotation;
import com.ksp.eservices.model.productTypeDetailsById.PRODUCT_DATA;
import com.ksp.eservices.model.subProductCategoryDetailsById.SubProductCategoryDetailsByIdRes;
import com.ksp.eservices.model.subProductCategoryDetailsById.Sub_product_master;
import com.ksp.eservices.network.RestApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListByCatId_Adapter extends RecyclerView.Adapter<ProductListByCatId_Adapter.ProductCatIdHolder> {

    Context context;
    List<PRODUCT_DATA> product_data;
    private Dialog Catalogue_Sublist_Dialog;
    private RecyclerView sub_catid_recycle;
    List<Sub_product_master> sub_product_masterList;

    public ProductListByCatId_Adapter(Context context, List<PRODUCT_DATA> product_data) {
        this.context = context;
        this.product_data = product_data;
    }

    @NonNull
    @Override
    public ProductCatIdHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalogue_list_view, parent, false);
        return new ProductCatIdHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductCatIdHolder holder, int position) {

        holder.catal_names.setText(product_data.get(position).getPrd_name());
        Glide.with(context).load(R.drawable.lappy1).into(holder.catal_image);

        holder.catal_card.setOnClickListener(v -> {
            CatalogueDialog(product_data.get(position).getPrd_name(), product_data.get(position).getPrd_id());
            SharedPreferences preferences = context.getSharedPreferences("SendSnap",Context.MODE_PRIVATE);
            preferences.edit().putString("ProductName",product_data.get(position).getPrd_name()).apply();
            preferences.edit().putString("ProdId",product_data.get(position).getPrd_id()).apply();
        });


    }


    private void CatalogueDialog(String prd_name, String prd_id) {

        Catalogue_Sublist_Dialog = new Dialog(context);
        Catalogue_Sublist_Dialog.setCancelable(true);
        Catalogue_Sublist_Dialog.setContentView(R.layout.catalogue_sublist_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Catalogue_Sublist_Dialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.alphaBlack)));
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        Catalogue_Sublist_Dialog.getWindow().setAttributes(lp);
        AppCompatImageView close = Catalogue_Sublist_Dialog.findViewById(R.id.img_close);
        TextView cat_descr = Catalogue_Sublist_Dialog.findViewById(R.id.cat_head_txt);
        sub_catid_recycle = Catalogue_Sublist_Dialog.findViewById(R.id.sub_catid_recy);
        cat_descr.setText(prd_name);

        //Product List By Cat Id API
        SubProductListByCatIdAPI(prd_id);

        close.setOnClickListener(v -> Catalogue_Sublist_Dialog.dismiss());
        Catalogue_Sublist_Dialog.show();

    }

    private void SubProductListByCatIdAPI(String prd_id) {

        Call<SubProductCategoryDetailsByIdRes> detailsByIdResCall = RestApi.get().getRestService().sub_prd_response_Call(prd_id);
        detailsByIdResCall.enqueue(new Callback<SubProductCategoryDetailsByIdRes>() {
            @Override
            public void onResponse(Call<SubProductCategoryDetailsByIdRes> call, Response<SubProductCategoryDetailsByIdRes> response) {
                if (response.body().getStatus().equalsIgnoreCase("true")){
                    if (response.body().getSub_product_master().size()>0 && response.body().getSub_product_master()!=null) {
                        sub_product_masterList = response.body().getSub_product_master();
                        SUB_PRD_Catalogue_Adapter sub_prd_catalogue_adapter = new SUB_PRD_Catalogue_Adapter(context,sub_product_masterList);
                        sub_catid_recycle.setAdapter(sub_prd_catalogue_adapter);
                        sub_catid_recycle.setHasFixedSize(true);
                        sub_catid_recycle.setNestedScrollingEnabled(true);
                        sub_catid_recycle.setLayoutManager(new LinearLayoutManager(context));
                    }

                }
            }

            @Override
            public void onFailure(Call<SubProductCategoryDetailsByIdRes> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return product_data == null ? 0 : product_data.size();
    }

    public class ProductCatIdHolder extends RecyclerView.ViewHolder {
        AppCompatImageView catal_image;
        TextView catal_names;
        CardView catal_card;

        public ProductCatIdHolder(@NonNull View itemView) {
            super(itemView);
            catal_names = itemView.findViewById(R.id.catal_text);
            catal_image = itemView.findViewById(R.id.catal_img);
            catal_card = itemView.findViewById(R.id.catalogue_card);
        }
    }
}
