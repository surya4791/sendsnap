package com.ksp.eservices.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ksp.eservices.R;
import com.ksp.eservices.activity.GetQuotation;
import com.ksp.eservices.model.productCategoryList.Category_master;
import com.ksp.eservices.model.productTypeDetailsById.PRODUCT_DATA;
import com.ksp.eservices.model.productTypeDetailsById.ProductTypeDetailsByIdRes;
import com.ksp.eservices.model.subProductCategoryDetailsById.Sub_product_master;
import com.ksp.eservices.network.RestApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SUB_PRD_Catalogue_Adapter extends RecyclerView.Adapter<SUB_PRD_Catalogue_Adapter.CatalogueHolder> {

    Context context;
    List<Sub_product_master> catalogueLists;

    public SUB_PRD_Catalogue_Adapter(Context context, List<Sub_product_master> catalogueLists) {
        this.context = context;
        this.catalogueLists = catalogueLists;
    }

    @NonNull
    @Override
    public CatalogueHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_by_id_text,parent,false);
        return new CatalogueHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CatalogueHolder holder, int position) {

        holder.prd_list_desc.setText(catalogueLists.get(position).getSub_prd_name());
        holder.prd_list_desc.setOnClickListener(v -> {
            SharedPreferences preferences = context.getSharedPreferences("SendSnap",Context.MODE_PRIVATE);
            preferences.edit().putString("SubProductName",catalogueLists.get(position).getSub_prd_name()).apply();
            preferences.edit().putString("SubProdId",catalogueLists.get(position).getSub_prd_id()).apply();
            context.startActivity(new Intent(context, GetQuotation.class));
        });
    }

    @Override
    public int getItemCount() {
        return catalogueLists==null?0:catalogueLists.size();
    }

    public class CatalogueHolder extends RecyclerView.ViewHolder {

         TextView prd_list_desc;

        public CatalogueHolder(@NonNull View itemView) {
            super(itemView);
            prd_list_desc = itemView.findViewById(R.id.prd_list_desc);

        }
    }
}
