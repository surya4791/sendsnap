package com.ksp.eservices.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;

import java.util.List;

public class QuotationImageUploadAdapter extends RecyclerView.Adapter<QuotationImageUploadAdapter.QuotationImageHolder> {
    List<Uri> uriList;
    Context context;

    public QuotationImageUploadAdapter(List<Uri> uriList, Context context) {
        this.uriList = uriList;
        this.context = context;
    }

    @NonNull
    @Override
    public QuotationImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quotation_upload_img_adapter,parent,false);
        return new QuotationImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuotationImageHolder holder, int position) {

        Glide.with(context).load(uriList.get(position)).into(holder.upload_img);

    }

    @Override
    public int getItemCount() {
        return uriList==null?0:uriList.size();
    }

    public class QuotationImageHolder extends RecyclerView.ViewHolder {
        AppCompatImageView upload_img;

        public QuotationImageHolder(@NonNull View itemView) {
            super(itemView);
            upload_img = itemView.findViewById(R.id.img_images);
        }
    }
}
