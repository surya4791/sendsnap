package com.ksp.eservices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.google.android.material.card.MaterialCardView;
import com.ksp.eservices.Interface.CardInterface;
import com.ksp.eservices.R;
import com.ksp.eservices.model.CardItem;
import com.ksp.eservices.utils.WelcomeLibraryObjects;

import java.util.ArrayList;
import java.util.List;

import static com.ksp.eservices.utils.WelcomeLibraryObjects.setupItem;

public class CardPagerAdapter extends PagerAdapter  {

    private final WelcomeLibraryObjects.LibraryObject[] LIBRARIES = new WelcomeLibraryObjects.LibraryObject[]{
            new WelcomeLibraryObjects.LibraryObject(
                    R.drawable.ic_ecommerce,
                    "Strategy"
            ),
            new WelcomeLibraryObjects.LibraryObject(
                    R.drawable.ic_ecommerce,
                    "Design"
            ),
            new WelcomeLibraryObjects.LibraryObject(
                    R.drawable.ic_ecommerce,
                    "Development"
            ),
            new WelcomeLibraryObjects.LibraryObject(
                    R.drawable.ic_ecommerce,
                    "Quality Assurance"
            )
    };

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public CardPagerAdapter(final Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return  LIBRARIES.length;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;

            view = mLayoutInflater.inflate(R.layout.welcome_card, container, false);
            setupItem(view, LIBRARIES[position]);

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }

}
