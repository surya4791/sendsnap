package com.ksp.eservices.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;
import com.ksp.eservices.model.Categories;
import com.ksp.eservices.model.OrderItem;
import com.ksp.eservices.model.getCustomerDashboardData.DASH_ORDER_DATA;

import java.util.List;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyOrderHolder> {

    Context context;
    List<DASH_ORDER_DATA> orderItemList;

    public MyOrdersAdapter(Context context, List<DASH_ORDER_DATA> orderItems) {
        this.context = context;
        this.orderItemList = orderItems;
    }

    @NonNull
    @Override
    public MyOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_order_list,parent,false);
        return new MyOrderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrderHolder holder, int position) {

        Glide.with(context).load(orderItemList.get(position)).into(holder.ord_prd_img);
        holder.dash_ord_date.setText(orderItemList.get(position).getReq_datetime());
        holder.dash_ord_prd_name.setText(orderItemList.get(position).getPrd_name());
        holder.dash_ord_price.setText(orderItemList.get(position).getOffered_price());
        holder.dash_ord_status.setText(orderItemList.get(position).getStatus());

    }

    @Override
    public int getItemCount() {
        return orderItemList==null?0:orderItemList.size();
    }

    public class MyOrderHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ord_prd_img;
        TextView dash_ord_prd_name, dash_ord_date, dash_ord_price, dash_ord_status;
        public MyOrderHolder(@NonNull View itemView) {
            super(itemView);
            ord_prd_img = itemView.findViewById(R.id.dash_ord_prd_img);
            dash_ord_date = itemView.findViewById(R.id.dash_ord_date);
            dash_ord_prd_name = itemView.findViewById(R.id.dash_ord_prd_name);
            dash_ord_price = itemView.findViewById(R.id.dash_ord_price);
            dash_ord_status = itemView.findViewById(R.id.dash_ord_status);
        }
    }
}
