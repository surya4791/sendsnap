package com.ksp.eservices.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ksp.eservices.R;
import com.ksp.eservices.activity.PlaceOrder;
import com.ksp.eservices.model.getAllCustomerQutations.ALL_QUT_DATA;

import java.util.List;

public class AllQuotationAdapter extends RecyclerView.Adapter<AllQuotationAdapter.QuotationHolder> {

    Context qctx;
    List<ALL_QUT_DATA> allQuotationItems;

    public AllQuotationAdapter(Context qctx, List<ALL_QUT_DATA> allQuotationItems) {
        this.qctx = qctx;
        this.allQuotationItems = allQuotationItems;
    }

    @NonNull
    @Override
    public QuotationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_quotations,parent,false);
        return new QuotationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuotationHolder holder, int position) {

        Glide.with(qctx).load(allQuotationItems.get(position).getFilepath()).into(holder.all_qut_prd_img);
        holder.child_root.setOnClickListener(v -> qctx.startActivity(new Intent(qctx, PlaceOrder.class).putExtra("Quota_Ref",allQuotationItems.get(position).getRef_no())));
        holder.all_qut_date.setText(allQuotationItems.get(position).getReq_datetime());
        holder.all_qut_prd_name.setText(allQuotationItems.get(position).getPrd_name());
        holder.all_qut_retailers.setText(String.format("Retailers - %s", allQuotationItems.get(position).getTotal_retailer_cnt()));
        holder.all_qut_price.setText(String.format("Best Price - %s", allQuotationItems.get(position).getBest_price()));

    }

    @Override
    public int getItemCount() {
        return allQuotationItems==null?0:allQuotationItems.size();
    }

    public class QuotationHolder extends RecyclerView.ViewHolder {

        RelativeLayout child_root;
        AppCompatImageView all_qut_prd_img;
        AppCompatTextView all_qut_prd_name, all_qut_retailers, all_qut_price, all_qut_date;

        public QuotationHolder(@NonNull View itemView) {
            super(itemView);

            child_root = itemView.findViewById(R.id.child_root);
            all_qut_prd_img = itemView.findViewById(R.id.all_qut_prd_img);
            all_qut_prd_name = itemView.findViewById(R.id.all_qut_product_name);
            all_qut_retailers = itemView.findViewById(R.id.all_qut_retailers);
            all_qut_price = itemView.findViewById(R.id.all_qut_best_price);
            all_qut_date = itemView.findViewById(R.id.all_qut_date);
        }
    }
}
