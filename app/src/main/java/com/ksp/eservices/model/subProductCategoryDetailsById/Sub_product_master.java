package com.ksp.eservices.model.subProductCategoryDetailsById;

public class Sub_product_master {

    private String sub_prd_name;

    private String prd_id;

    private String sub_prd_id;

    public String getSub_prd_name ()
    {
        return sub_prd_name;
    }

    public void setSub_prd_name (String sub_prd_name)
    {
        this.sub_prd_name = sub_prd_name;
    }

    public String getPrd_id ()
    {
        return prd_id;
    }

    public void setPrd_id (String prd_id)
    {
        this.prd_id = prd_id;
    }

    public String getSub_prd_id ()
    {
        return sub_prd_id;
    }

    public void setSub_prd_id (String sub_prd_id)
    {
        this.sub_prd_id = sub_prd_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sub_prd_name = "+sub_prd_name+", prd_id = "+prd_id+", sub_prd_id = "+sub_prd_id+"]";
    }
}
