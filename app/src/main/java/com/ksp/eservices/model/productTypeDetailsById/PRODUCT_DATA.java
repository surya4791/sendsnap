package com.ksp.eservices.model.productTypeDetailsById;

public class PRODUCT_DATA {

    private String prd_name;

    private String cat_id;

    private String state_id;

    private String prd_id;

    private String country_id;

    public String getPrd_name ()
    {
        return prd_name;
    }

    public void setPrd_name (String prd_name)
    {
        this.prd_name = prd_name;
    }

    public String getCat_id ()
    {
        return cat_id;
    }

    public void setCat_id (String cat_id)
    {
        this.cat_id = cat_id;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getPrd_id ()
    {
        return prd_id;
    }

    public void setPrd_id (String prd_id)
    {
        this.prd_id = prd_id;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [prd_name = "+prd_name+", cat_id = "+cat_id+", state_id = "+state_id+", prd_id = "+prd_id+", country_id = "+country_id+"]";
    }
}
