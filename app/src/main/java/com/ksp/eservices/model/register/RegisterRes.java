package com.ksp.eservices.model.register;

public class RegisterRes {

    private String cust_name;

    private String message;

    private String customer_id;

    private String REQUEST_DETAILS;

    private String status;

    public String getCust_name ()
    {
        return cust_name;
    }

    public void setCust_name (String cust_name)
    {
        this.cust_name = cust_name;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getREQUEST_DETAILS ()
    {
        return REQUEST_DETAILS;
    }

    public void setREQUEST_DETAILS (String REQUEST_DETAILS)
    {
        this.REQUEST_DETAILS = REQUEST_DETAILS;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cust_name = "+cust_name+", message = "+message+", customer_id = "+customer_id+", REQUEST_DETAILS = "+REQUEST_DETAILS+", status = "+status+"]";
    }
}
