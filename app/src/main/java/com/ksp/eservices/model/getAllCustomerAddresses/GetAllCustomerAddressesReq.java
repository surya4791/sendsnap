package com.ksp.eservices.model.getAllCustomerAddresses;

public class GetAllCustomerAddressesReq {

    private String customer_id;

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [customer_id = "+customer_id+"]";
    }
}
