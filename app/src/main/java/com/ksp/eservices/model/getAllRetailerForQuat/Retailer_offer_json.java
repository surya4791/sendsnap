package com.ksp.eservices.model.getAllRetailerForQuat;

public class Retailer_offer_json {

    private String deliverytime;

    private String discount;

    private String warrenty;

    public String getDeliverytime ()
    {
        return deliverytime;
    }

    public void setDeliverytime (String deliverytime)
    {
        this.deliverytime = deliverytime;
    }

    public String getDiscount ()
    {
        return discount;
    }

    public void setDiscount (String discount)
    {
        this.discount = discount;
    }

    public String getWarrenty ()
    {
        return warrenty;
    }

    public void setWarrenty (String warrenty)
    {
        this.warrenty = warrenty;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [deliverytime = "+deliverytime+", discount = "+discount+", warrenty = "+warrenty+"]";
    }
}
