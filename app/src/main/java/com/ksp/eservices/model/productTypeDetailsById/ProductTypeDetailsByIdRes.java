package com.ksp.eservices.model.productTypeDetailsById;

import java.util.List;

public class ProductTypeDetailsByIdRes {

    private List<PRODUCT_DATA> PRODUCT_DATA;

    private String message;

    private String status;

    public List<PRODUCT_DATA> getPRODUCT_DATA ()
    {
        return PRODUCT_DATA;
    }

    public void setPRODUCT_DATA (List<PRODUCT_DATA> PRODUCT_DATA)
    {
        this.PRODUCT_DATA = PRODUCT_DATA;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PRODUCT_DATA = "+PRODUCT_DATA+", message = "+message+", status = "+status+"]";
    }
}
