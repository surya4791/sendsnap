package com.ksp.eservices.model.insertCustomerAddress;

public class InsertCustomerAddressRes {

    private String txn_ref_no;

    private String message;

    private String status;

    public String getTxn_ref_no ()
    {
        return txn_ref_no;
    }

    public void setTxn_ref_no (String txn_ref_no)
    {
        this.txn_ref_no = txn_ref_no;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [txn_ref_no = "+txn_ref_no+", message = "+message+", status = "+status+"]";
    }
}
