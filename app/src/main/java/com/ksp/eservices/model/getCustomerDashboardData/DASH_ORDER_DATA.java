package com.ksp.eservices.model.getCustomerDashboardData;

public class DASH_ORDER_DATA {

    private String prd_name;

    private String order_ref_no;

    private String ref_no;

    private String req_datetime;

    private String offered_price;

    private String status;

    public String getPrd_name ()
    {
        return prd_name;
    }

    public void setPrd_name (String prd_name)
    {
        this.prd_name = prd_name;
    }

    public String getOrder_ref_no ()
    {
        return order_ref_no;
    }

    public void setOrder_ref_no (String order_ref_no)
    {
        this.order_ref_no = order_ref_no;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getReq_datetime ()
    {
        return req_datetime;
    }

    public void setReq_datetime (String req_datetime)
    {
        this.req_datetime = req_datetime;
    }

    public String getOffered_price ()
    {
        return offered_price;
    }

    public void setOffered_price (String offered_price)
    {
        this.offered_price = offered_price;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [prd_name = "+prd_name+", order_ref_no = "+order_ref_no+", ref_no = "+ref_no+", req_datetime = "+req_datetime+", offered_price = "+offered_price+", status = "+status+"]";
    }

}
