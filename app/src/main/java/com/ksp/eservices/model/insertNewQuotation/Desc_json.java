package com.ksp.eservices.model.insertNewQuotation;

public class Desc_json {

    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [desc = "+desc+"]";
    }
}
