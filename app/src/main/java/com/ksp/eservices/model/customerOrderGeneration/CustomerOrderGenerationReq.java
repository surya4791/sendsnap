package com.ksp.eservices.model.customerOrderGeneration;

public class CustomerOrderGenerationReq {

    private String address_ref_no;

    private String retailer_id;

    private String lat_no;

    private String ref_no;

    private String state_id;

    private String district_id;

    private String cust_id;

    private String long_no;

    public String getAddress_ref_no ()
    {
        return address_ref_no;
    }

    public void setAddress_ref_no (String address_ref_no)
    {
        this.address_ref_no = address_ref_no;
    }

    public String getRetailer_id ()
    {
        return retailer_id;
    }

    public void setRetailer_id (String retailer_id)
    {
        this.retailer_id = retailer_id;
    }

    public String getLat_no ()
    {
        return lat_no;
    }

    public void setLat_no (String lat_no)
    {
        this.lat_no = lat_no;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getDistrict_id ()
    {
        return district_id;
    }

    public void setDistrict_id (String district_id)
    {
        this.district_id = district_id;
    }

    public String getCust_id ()
    {
        return cust_id;
    }

    public void setCust_id (String cust_id)
    {
        this.cust_id = cust_id;
    }

    public String getLong_no ()
    {
        return long_no;
    }

    public void setLong_no (String long_no)
    {
        this.long_no = long_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [address_ref_no = "+address_ref_no+", retailer_id = "+retailer_id+", lat_no = "+lat_no+", ref_no = "+ref_no+", state_id = "+state_id+", district_id = "+district_id+", cust_id = "+cust_id+", long_no = "+long_no+"]";
    }
}
