package com.ksp.eservices.model.getCustomerDashboardData;

public class DASH_QUT_DATA {
    private String res_count;

    private String prd_name;

    private String filepath;

    private String cat_desc;

    private String rec_status;

    private String total_retailer_cnt;

    private String ref_no;

    private String req_datetime;

    private String best_price;

    private String status;

    public String getRes_count ()
    {
        return res_count;
    }

    public void setRes_count (String res_count)
    {
        this.res_count = res_count;
    }

    public String getPrd_name ()
    {
        return prd_name;
    }

    public void setPrd_name (String prd_name)
    {
        this.prd_name = prd_name;
    }

    public String getFilepath ()
    {
        return filepath;
    }

    public void setFilepath (String filepath)
    {
        this.filepath = filepath;
    }

    public String getCat_desc ()
    {
        return cat_desc;
    }

    public void setCat_desc (String cat_desc)
    {
        this.cat_desc = cat_desc;
    }

    public String getRec_status ()
    {
        return rec_status;
    }

    public void setRec_status (String rec_status)
    {
        this.rec_status = rec_status;
    }

    public String getTotal_retailer_cnt ()
    {
        return total_retailer_cnt;
    }

    public void setTotal_retailer_cnt (String total_retailer_cnt)
    {
        this.total_retailer_cnt = total_retailer_cnt;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getReq_datetime ()
    {
        return req_datetime;
    }

    public void setReq_datetime (String req_datetime)
    {
        this.req_datetime = req_datetime;
    }

    public String getBest_price ()
    {
        return best_price;
    }

    public void setBest_price (String best_price)
    {
        this.best_price = best_price;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [res_count = "+res_count+", prd_name = "+prd_name+", filepath = "+filepath+", cat_desc = "+cat_desc+", rec_status = "+rec_status+", total_retailer_cnt = "+total_retailer_cnt+", ref_no = "+ref_no+", req_datetime = "+req_datetime+", best_price = "+best_price+", status = "+status+"]";
    }
}
