package com.ksp.eservices.model.getAllRetailerForQuat;

import java.util.List;

public class GetAllRetailerForQuatRes {

    private List<RETAILER_DATA> RETAILER_DATA;

    private List<QUT_DATA> QUT_DATA;

    private String message;

    private String status;

    public List<RETAILER_DATA> getRETAILER_DATA ()
    {
        return RETAILER_DATA;
    }

    public void setRETAILER_DATA (List<RETAILER_DATA> RETAILER_DATA)
    {
        this.RETAILER_DATA = RETAILER_DATA;
    }

    public List<QUT_DATA> getQUT_DATA ()
    {
        return QUT_DATA;
    }

    public void setQUT_DATA (List<QUT_DATA> QUT_DATA)
    {
        this.QUT_DATA = QUT_DATA;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RETAILER_DATA = "+RETAILER_DATA+", QUT_DATA = "+QUT_DATA+", message = "+message+", status = "+status+"]";
    }
}
