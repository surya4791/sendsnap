package com.ksp.eservices.model.insertCustomerAddress;

public class InsertCustomerAddressReq {

    private String cust_id;
    private String name;
    private String mobile;
    private String email;
    private String house_no;
    private String street;
    private String city;
    private String land_mark;
    private String pin_code;
    private String country_id;
    private String state_id;
    private String district_id;
    private String address_type;

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLand_mark() {
        return land_mark;
    }

    public void setLand_mark(String land_mark) {
        this.land_mark = land_mark;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cust_id = "+cust_id+", name = "+name+", mobile = "+mobile+", email= "+email+", house_no = "+house_no+"," +
                ",street = "+street+", city = "+city+", land_mark = "+land_mark+", pin_code = "+pin_code+", country_id = "+country_id+", state_id = "+state_id+", district_id = "+district_id+", address_type = "+address_type+"]";
    }
}
