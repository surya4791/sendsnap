package com.ksp.eservices.model.otp;

public class OTPReq {

    private String mobile_no;

    private String otp;

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [mobile_no = "+mobile_no+", otp = "+otp+"]";
    }
}
