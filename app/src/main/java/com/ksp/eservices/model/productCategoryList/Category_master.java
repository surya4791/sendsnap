package com.ksp.eservices.model.productCategoryList;

public class Category_master {

    private String cat_desc;

    private String cat_id;

    private String country_id;

    public String getCat_desc ()
    {
        return cat_desc;
    }

    public void setCat_desc (String cat_desc)
    {
        this.cat_desc = cat_desc;
    }

    public String getCat_id ()
    {
        return cat_id;
    }

    public void setCat_id (String cat_id)
    {
        this.cat_id = cat_id;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cat_desc = "+cat_desc+", cat_id = "+cat_id+", country_id = "+country_id+"]";
    }
}
