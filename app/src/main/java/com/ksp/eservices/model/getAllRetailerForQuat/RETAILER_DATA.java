package com.ksp.eservices.model.getAllRetailerForQuat;

public class RETAILER_DATA {

    private String phone_no;

    private String retailer_branch_name;

    private String contact_person;

    private String address_1;

    private String address_2;

    private String mobile_no;

    private String retailer_id;

    private String retailer_name;

    private String offered_price;

    private Retailer_offer_json retailer_offer_json;

    public String getPhone_no ()
    {
        return phone_no;
    }

    public void setPhone_no (String phone_no)
    {
        this.phone_no = phone_no;
    }

    public String getRetailer_branch_name ()
    {
        return retailer_branch_name;
    }

    public void setRetailer_branch_name (String retailer_branch_name)
    {
        this.retailer_branch_name = retailer_branch_name;
    }

    public String getContact_person ()
    {
        return contact_person;
    }

    public void setContact_person (String contact_person)
    {
        this.contact_person = contact_person;
    }

    public String getAddress_1 ()
    {
        return address_1;
    }

    public void setAddress_1 (String address_1)
    {
        this.address_1 = address_1;
    }

    public String getAddress_2 ()
    {
        return address_2;
    }

    public void setAddress_2 (String address_2)
    {
        this.address_2 = address_2;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getRetailer_id ()
    {
        return retailer_id;
    }

    public void setRetailer_id (String retailer_id)
    {
        this.retailer_id = retailer_id;
    }

    public String getRetailer_name ()
    {
        return retailer_name;
    }

    public void setRetailer_name (String retailer_name)
    {
        this.retailer_name = retailer_name;
    }

    public String getOffered_price ()
    {
        return offered_price;
    }

    public void setOffered_price (String offered_price)
    {
        this.offered_price = offered_price;
    }

    public Retailer_offer_json getRetailer_offer_json ()
    {
        return retailer_offer_json;
    }

    public void setRetailer_offer_json (Retailer_offer_json retailer_offer_json)
    {
        this.retailer_offer_json = retailer_offer_json;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [phone_no = "+phone_no+", retailer_branch_name = "+retailer_branch_name+", contact_person = "+contact_person+", address_1 = "+address_1+", address_2 = "+address_2+", mobile_no = "+mobile_no+", retailer_id = "+retailer_id+", retailer_name = "+retailer_name+", offered_price = "+offered_price+", retailer_offer_json = "+retailer_offer_json+"]";
    }
}
