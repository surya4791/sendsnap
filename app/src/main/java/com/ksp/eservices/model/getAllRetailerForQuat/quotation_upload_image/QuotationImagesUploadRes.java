package com.ksp.eservices.model.getAllRetailerForQuat.quotation_upload_image;

public class QuotationImagesUploadRes {

    private String code;

    private String ref_no;

    private String message;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", ref_no = "+ref_no+", message = "+message+"]";
    }
}
