package com.ksp.eservices.model.register;

public class RegisterReq {

    private String fulladdress;

    private String stateLongName;

    private String tokenid;

    private String mobile_no;

    private String lat_no;

    private String customer_name;

    private String long_no;

    public String getFulladdress ()
    {
        return fulladdress;
    }

    public void setFulladdress (String fulladdress)
    {
        this.fulladdress = fulladdress;
    }

    public String getStateLongName ()
    {
        return stateLongName;
    }

    public void setStateLongName (String stateLongName)
    {
        this.stateLongName = stateLongName;
    }

    public String getTokenid ()
    {
        return tokenid;
    }

    public void setTokenid (String tokenid)
    {
        this.tokenid = tokenid;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    public String getLat_no ()
    {
        return lat_no;
    }

    public void setLat_no (String lat_no)
    {
        this.lat_no = lat_no;
    }

    public String getCustomer_name ()
    {
        return customer_name;
    }

    public void setCustomer_name (String customer_name)
    {
        this.customer_name = customer_name;
    }

    public String getLong_no ()
    {
        return long_no;
    }

    public void setLong_no (String long_no)
    {
        this.long_no = long_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [fulladdress = "+fulladdress+", stateLongName = "+stateLongName+", tokenid = "+tokenid+", mobile_no = "+mobile_no+", lat_no = "+lat_no+", customer_name = "+customer_name+", long_no = "+long_no+"]";
    }
}
