package com.ksp.eservices.model.getAllCustomerAddresses;

import java.util.List;

public class GetAllCustomerAddressesRes {

    private List<ADDRESS_DATA> ADDRESS_DATA;

    private String status;

    public List<ADDRESS_DATA> getADDRESS_DATA ()
    {
        return ADDRESS_DATA;
    }

    public void setADDRESS_DATA (List<ADDRESS_DATA> ADDRESS_DATA)
    {
        this.ADDRESS_DATA = ADDRESS_DATA;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ADDRESS_DATA = "+ADDRESS_DATA+", status = "+status+"]";
    }
}
