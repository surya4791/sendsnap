package com.ksp.eservices.model.subProductCategoryDetailsById;

import java.util.List;

public class SubProductCategoryDetailsByIdRes {

    private List<Sub_product_master> sub_product_master;

    private String message;

    private String status;

    public List<Sub_product_master> getSub_product_master ()
    {
        return sub_product_master;
    }

    public void setSub_product_master (List<Sub_product_master> sub_product_master)
    {
        this.sub_product_master = sub_product_master;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sub_product_master = "+sub_product_master+", message = "+message+", status = "+status+"]";
    }
}
