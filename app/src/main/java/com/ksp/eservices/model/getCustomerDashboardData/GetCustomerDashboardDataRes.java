package com.ksp.eservices.model.getCustomerDashboardData;

import com.ksp.eservices.model.getAllRetailerForQuat.QUT_DATA;

import java.util.List;

public class GetCustomerDashboardDataRes {

    private List<DASH_ORDER_DATA> ORDER_DATA;

    private List<DASH_QUT_DATA> QUT_DATA;

    private String message;

    private String status;

    public List<DASH_ORDER_DATA> getORDER_DATA ()
    {
        return ORDER_DATA;
    }

    public void setORDER_DATA (List<DASH_ORDER_DATA> ORDER_DATA)
    {
        this.ORDER_DATA = ORDER_DATA;
    }

    public List<DASH_QUT_DATA> getQUT_DATA ()
    {
        return QUT_DATA;
    }

    public void setQUT_DATA (List<DASH_QUT_DATA> QUT_DATA)
    {
        this.QUT_DATA = QUT_DATA;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ORDER_DATA = "+ORDER_DATA+", QUT_DATA = "+QUT_DATA+", message = "+message+", status = "+status+"]";
    }
}
