package com.ksp.eservices.model.getAllCustomerAddresses;

public class ADDRESS_DATA {

    private String land_mark;

    private String city;

    private String street;

    private String pin_code;

    private String name;

    private String house_no;

    private String ref_no;

    public String getLand_mark ()
    {
        return land_mark;
    }

    public void setLand_mark (String land_mark)
    {
        this.land_mark = land_mark;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getPin_code ()
    {
        return pin_code;
    }

    public void setPin_code (String pin_code)
    {
        this.pin_code = pin_code;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getHouse_no ()
    {
        return house_no;
    }

    public void setHouse_no (String house_no)
    {
        this.house_no = house_no;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [land_mark = "+land_mark+", city = "+city+", street = "+street+", pin_code = "+pin_code+", name = "+name+", house_no = "+house_no+", ref_no = "+ref_no+"]";
    }
}
