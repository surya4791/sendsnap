package com.ksp.eservices.model.getCustomerAddressByTxn;

public class TXN_ADDRESS_DATA {

    private String land_mark;

    private String city;

    private String street;

    private String pin_code;

    private String name;

    private String house_no;

    private String mobile;

    private String ref_no;

    private String state_id;

    private String district_id;

    private String country_id;

    private String email;

    public String getLand_mark ()
    {
        return land_mark;
    }

    public void setLand_mark (String land_mark)
    {
        this.land_mark = land_mark;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getStreet ()
    {
        return street;
    }

    public void setStreet (String street)
    {
        this.street = street;
    }

    public String getPin_code ()
    {
        return pin_code;
    }

    public void setPin_code (String pin_code)
    {
        this.pin_code = pin_code;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getHouse_no ()
    {
        return house_no;
    }

    public void setHouse_no (String house_no)
    {
        this.house_no = house_no;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getDistrict_id ()
    {
        return district_id;
    }

    public void setDistrict_id (String district_id)
    {
        this.district_id = district_id;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [land_mark = "+land_mark+", city = "+city+", street = "+street+", pin_code = "+pin_code+", name = "+name+", house_no = "+house_no+", mobile = "+mobile+", ref_no = "+ref_no+", state_id = "+state_id+", district_id = "+district_id+", country_id = "+country_id+", email = "+email+"]";
    }
}
