package com.ksp.eservices.model.verifygeolocation;

public class VerifyLocationReq {

    private String fulladdress;

    private String stateLongName;

    public String getFulladdress ()
    {
        return fulladdress;
    }

    public void setFulladdress (String fulladdress)
    {
        this.fulladdress = fulladdress;
    }

    public String getStateLongName ()
    {
        return stateLongName;
    }

    public void setStateLongName (String stateLongName)
    {
        this.stateLongName = stateLongName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [fulladdress = "+fulladdress+", stateLongName = "+stateLongName+"]";
    }
}
