package com.ksp.eservices.model;

public class Categories {

    private int image;
    private String category_name;

    public Categories(int image, String category_name) {
        this.image = image;
        this.category_name = category_name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
