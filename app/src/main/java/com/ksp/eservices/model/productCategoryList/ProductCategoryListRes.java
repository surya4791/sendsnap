package com.ksp.eservices.model.productCategoryList;

import java.util.List;

public class ProductCategoryListRes {

    private List<Category_master> category_master;

    private String message;

    private String status;

    public List<Category_master> getCategory_master ()
    {
        return category_master;
    }

    public void setCategory_master (List<Category_master> category_master)
    {
        this.category_master = category_master;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [category_master = "+category_master+", message = "+message+", status = "+status+"]";
    }
}
