package com.ksp.eservices.model;

public class ShoppingImageItem {

    private int Image;
    private String img_name;

    public ShoppingImageItem(int image, String img_name) {
        Image = image;
        this.img_name = img_name;
    }
}
