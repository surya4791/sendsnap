package com.ksp.eservices.model.login;

public class LoginReq {

    private String mobile_no;

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [mobile_no = "+mobile_no+"]";
    }
}
