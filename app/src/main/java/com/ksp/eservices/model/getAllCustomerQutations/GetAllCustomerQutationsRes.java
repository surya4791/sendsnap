package com.ksp.eservices.model.getAllCustomerQutations;

import java.util.List;

public class GetAllCustomerQutationsRes {

    private List<ALL_QUT_DATA> QUT_DATA;

    private String message;

    private String status;

    public List<ALL_QUT_DATA> getQUT_DATA ()
    {
        return QUT_DATA;
    }

    public void setQUT_DATA (List<ALL_QUT_DATA> QUT_DATA)
    {
        this.QUT_DATA = QUT_DATA;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [QUT_DATA = "+QUT_DATA+", message = "+message+", status = "+status+"]";
    }
}
