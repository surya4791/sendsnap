package com.ksp.eservices.model.login;

public class LoginRes {

    private String code;

    private String cust_type;

    private String cust_name;

    private String customer_id;

    private String message;

    private String status;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getCust_type ()
    {
        return cust_type;
    }

    public void setCust_type (String cust_type)
    {
        this.cust_type = cust_type;
    }

    public String getCust_name ()
    {
        return cust_name;
    }

    public void setCust_name (String cust_name)
    {
        this.cust_name = cust_name;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", cust_type = "+cust_type+", cust_name = "+cust_name+", customer_id = "+customer_id+", message = "+message+", status = "+status+"]";
    }
}
