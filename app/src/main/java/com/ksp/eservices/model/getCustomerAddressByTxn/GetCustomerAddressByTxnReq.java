package com.ksp.eservices.model.getCustomerAddressByTxn;

public class GetCustomerAddressByTxnReq {

    private String ref_no;

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ref_no = "+ref_no+"]";
    }
}
