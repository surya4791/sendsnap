package com.ksp.eservices.model.insertNewQuotation;

public class InsertNewQuotationReq {

    private String input_type;

    private String ref_no;

    private String prd_id;

    private String sub_prd_id;

    private String long_no;

    private Desc_json desc_json;

    private String brand_id;

    private String service_type;

    private String cat_id;

    private String lat_no;

    private String state_id;

    private String district_id;

    private String cust_id;

    public String getInput_type ()
    {
        return input_type;
    }

    public void setInput_type (String input_type)
    {
        this.input_type = input_type;
    }

    public String getRef_no ()
    {
        return ref_no;
    }

    public void setRef_no (String ref_no)
    {
        this.ref_no = ref_no;
    }

    public String getPrd_id ()
    {
        return prd_id;
    }

    public void setPrd_id (String prd_id)
    {
        this.prd_id = prd_id;
    }

    public String getSub_prd_id ()
    {
        return sub_prd_id;
    }

    public void setSub_prd_id (String sub_prd_id)
    {
        this.sub_prd_id = sub_prd_id;
    }

    public String getLong_no ()
    {
        return long_no;
    }

    public void setLong_no (String long_no)
    {
        this.long_no = long_no;
    }

    public Desc_json getDesc_json ()
    {
        return desc_json;
    }

    public void setDesc_json (Desc_json desc_json)
    {
        this.desc_json = desc_json;
    }

    public String getBrand_id ()
    {
        return brand_id;
    }

    public void setBrand_id (String brand_id)
    {
        this.brand_id = brand_id;
    }

    public String getService_type ()
    {
        return service_type;
    }

    public void setService_type (String service_type)
    {
        this.service_type = service_type;
    }

    public String getCat_id ()
    {
        return cat_id;
    }

    public void setCat_id (String cat_id)
    {
        this.cat_id = cat_id;
    }

    public String getLat_no ()
    {
        return lat_no;
    }

    public void setLat_no (String lat_no)
    {
        this.lat_no = lat_no;
    }

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getDistrict_id ()
    {
        return district_id;
    }

    public void setDistrict_id (String district_id)
    {
        this.district_id = district_id;
    }

    public String getCust_id ()
    {
        return cust_id;
    }

    public void setCust_id (String cust_id)
    {
        this.cust_id = cust_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [input_type = "+input_type+", ref_no = "+ref_no+", prd_id = "+prd_id+", sub_prd_id = "+sub_prd_id+", long_no = "+long_no+", desc_json = "+desc_json+", brand_id = "+brand_id+", service_type = "+service_type+", cat_id = "+cat_id+", lat_no = "+lat_no+", state_id = "+state_id+", district_id = "+district_id+", cust_id = "+cust_id+"]";
    }
}
