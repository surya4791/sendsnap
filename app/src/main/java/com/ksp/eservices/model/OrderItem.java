package com.ksp.eservices.model;

public class OrderItem {

    private int image;
    private String item_name;
    private String status;

    public OrderItem(int image, String item_name, String status) {
        this.image = image;
        this.item_name = item_name;
        this.status = status;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
