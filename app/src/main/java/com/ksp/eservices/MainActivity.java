package com.ksp.eservices;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.button.MaterialButton;
import com.ksp.eservices.activity.WelcomeScreen;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    BlurView card_blur;
    float radius = 20f;
    Drawable windowBackground;
    ViewGroup root;
    AppCompatButton login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        root = findViewById(R.id.root);
        card_blur = findViewById(R.id.BlurView);
        login = findViewById(R.id.login);

        login.setOnClickListener(this);

        windowBackground = getWindow().getDecorView().getBackground();

        card_blur.setupWith(root)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(this))
                .setBlurRadius(radius)
                .setHasFixedTransformationMatrix(true);

    }

    @Override
    public void onClick(View v) {
        if (v==login){
            startActivity(new Intent(MainActivity.this, WelcomeScreen.class));
        }
    }
}
