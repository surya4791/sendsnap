package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ksp.eservices.R;
import com.ksp.eservices.adapter.AllQuotationAdapter;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.getAllCustomerQutations.GetAllCustomerQutationsRes;
import com.ksp.eservices.network.RestApi;
import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllQuotation extends BaseActivity {

    RecyclerView quota_recycle;
    private SharedPreferences preferences;
    private AppCompatTextView no_quot_avail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Hooks
        setContentView(R.layout.quotationed_list);
        quota_recycle = findViewById(R.id.all_quota_recycle);
        no_quot_avail = findViewById(R.id.no_quot);

        GetAllCustomerQuotationAPI();

    }

    private void GetAllCustomerQuotationAPI() {
        showProgress();

        Call<GetAllCustomerQutationsRes> qutationsResCall = RestApi.get().getRestService().custquotationResponseCall(preferences.getString("cust_Id",null));
        qutationsResCall.enqueue(new Callback<GetAllCustomerQutationsRes>() {
            @Override
            public void onResponse(Call<GetAllCustomerQutationsRes> call, Response<GetAllCustomerQutationsRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getQUT_DATA().size()>0){
                            no_quot_avail.setVisibility(View.GONE);
                            quota_recycle.setVisibility(View.VISIBLE);

                            AllQuotationAdapter allQuotationAdapter = new AllQuotationAdapter(ViewAllQuotation.this,response.body().getQUT_DATA());
                            quota_recycle.setAdapter(allQuotationAdapter);
                            quota_recycle.setHasFixedSize(true);
                            quota_recycle.setLayoutManager(new LinearLayoutManager(ViewAllQuotation.this));
                            quota_recycle.setNestedScrollingEnabled(true);
                        }else {
                            no_quot_avail.setVisibility(View.VISIBLE);
                            quota_recycle.setVisibility(View.GONE);
                        }
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetAllCustomerQutationsRes> call, Throwable t) {

            }
        });
    }
}
