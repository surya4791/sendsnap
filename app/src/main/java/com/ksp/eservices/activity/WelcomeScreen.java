package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.viewpager.widget.ViewPager;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.CardFragmentPagerAdapter;
import com.ksp.eservices.adapter.CardPagerAdapter;
import com.ksp.eservices.model.CardItem;
import com.ksp.eservices.utils.ShadowTransformer;

import eightbitlab.com.blurview.BlurView;

public class WelcomeScreen extends AppCompatActivity implements View.OnClickListener {

    AppCompatButton wel_get_started;
    private ViewFlipper viewFlipper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Hooks
        setContentView(R.layout.activity_welcome_screen1);
       /* infiniteCycleViewPager = findViewById(R.id.viewpager);
        wel_root = findViewById(R.id.wel_root);*/
        wel_get_started = findViewById(R.id.get_started);
        viewFlipper = findViewById(R.id.vflip);
       // card_blur = findViewById(R.id.WBlurView);

        //Adapters
       /* mCardAdapter = new CardPagerAdapter(WelcomeScreen.this);
        infiniteCycleViewPager.setAdapter(mCardAdapter);
        infiniteCycleViewPager.startAutoScroll(true);
        windowBackground = getWindow().getDecorView().getBackground();*/

        //Click Listener Event
        wel_get_started.setOnClickListener(this);


        int[] images = {R.drawable.home_appli,R.drawable.plant,R.drawable.plant1};

        for (int image:images){
            flipperImages(image);
        }


    }

    private void flipperImages(int image) {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(4000);//4sec
        viewFlipper.setAutoStart(true);

        //animation

        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        if(v==wel_get_started) startActivity(new Intent(WelcomeScreen.this,LoginActivity.class));
    }
}
