package com.ksp.eservices.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.karumi.dexter.BuildConfig;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.DexterBuilder;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.QuotationImageUploadAdapter;
import com.ksp.eservices.model.getAllRetailerForQuat.quotation_upload_image.QuotationImagesUploadRes;
import com.ksp.eservices.model.insertNewQuotation.Desc_json;
import com.ksp.eservices.model.insertNewQuotation.InsertNewQuotationReq;
import com.ksp.eservices.model.insertNewQuotation.InsertNewQuotationRes;
import com.ksp.eservices.network.RestApi;
import com.ksp.eservices.utils.AlertDialogManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetQuotation extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = GetQuotation.class.getSimpleName();
    private ViewFlipper viewFlipper;
    private MaterialButton camera, gallery;
    private AppCompatButton get_quotation;
    private int PICK_CAMERA = 2;
    public static final int MEDIA_TYPE_IMAGE =1;
    private RecyclerView upload_img_recycle;
    private List<Uri> uriList = new ArrayList<>();
    List<String> imagesEncodedList=new ArrayList<>();
    List<String> imagesList=new ArrayList<>();
    int count = 0;
    RelativeLayout parent_root;
    MaterialTextView select_catalogue_txt, select_sub_catalogue_txt;
    private SharedPreferences preferences;
    String selected_item = "";
    private Dialog successDialog;
    private TextInputEditText prod_desc_edt;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_quotation1);

        //Hooks
        viewFlipper = findViewById(R.id.flip_refer_images);
        camera = findViewById(R.id.camera);
        gallery = findViewById(R.id.gallery);
        get_quotation = findViewById(R.id.get_quotation);
        upload_img_recycle = findViewById(R.id.upload_img_recycle);
        parent_root = findViewById(R.id.root);
        select_catalogue_txt = findViewById(R.id.select_catalogue_txt);
        select_sub_catalogue_txt = findViewById(R.id.select_sub_catalogue_txt);
        prod_desc_edt = findViewById(R.id.prd_Desc_edt);

        //SharedPreference
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Preference is edited from ProductListCatID_Adapter Class
        if (preferences.getString("ProductName",null)!=null){
            select_catalogue_txt.setText(preferences.getString("ProductName",null));
        }
        //Preference is edited from SUB_PRD_Catalogue_Adapter Class
        if (preferences.getString("SubProductName",null)!=null){
            select_sub_catalogue_txt.setText(preferences.getString("SubProductName",null));
        }


        //OnClick Listener
        camera.setOnClickListener(this);
        gallery.setOnClickListener(this);
        get_quotation.setOnClickListener(this);

        int[] images = {R.drawable.img1,R.drawable.img2,R.drawable.img3,R.drawable.img4};

        for (int image:images){
            flipperImages(image);
        }


    }

    private void flipperImages(int image) {

        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(4000);//4sec
        viewFlipper.setAutoStart(true);

        //animation

        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        if (v == camera){

            if (checkCameraPermissions(GetQuotation.this)) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, PICK_CAMERA);
            }else {
                requireCameraPermission();
            }

        }else if (v == gallery){
            if (checkCameraPermissions(GetQuotation.this)) {

                Intent intent = new Intent();
                intent.setType("image/*");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
            }else {
                requireGalleryPermission();
            }

        }else if (v == get_quotation){

            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            builder.setTitle("Choose Service Type");
            final String[] services = new String[]{
                    "BUY",
                    "SELL",
                    "SERVICE",
            };
            builder.setSingleChoiceItems(services,-1,(dialog, which) -> {
                selected_item = Arrays.asList(services).get(which);
            });
            builder.setPositiveButton("Okay", (dialog, which) -> {
                dialog.dismiss();
                QuotationImagesUploadAPI();
            });
            builder.setNegativeButton("Cancel",(dialog, which) -> dialog.dismiss());
            builder.create();
            builder.show();


        }
    }

    private boolean checkCameraPermissions(GetQuotation getQuotation) {
        return ActivityCompat.checkSelfPermission(getQuotation, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getQuotation, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getQuotation, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requireCameraPermission() {
        Dexter.withContext(this).withPermission(Manifest.permission.CAMERA).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, PICK_CAMERA);

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                if (permissionDeniedResponse.isPermanentlyDenied()){
                    showPermissionsAlert();
                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                permissionToken.continuePermissionRequest();

            }
        }).onSameThread().check();
    }


    private void requireGalleryPermission() {

        DexterBuilder.MultiPermissionListener builder =
                Dexter.withContext(this).
                        withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE);
        builder.withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()){
                    if (GetQuotation.MEDIA_TYPE_IMAGE == 1){

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        }
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
                    }
                }else if (report.isAnyPermissionPermanentlyDenied()){
                    showPermissionsAlert();
                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();

            }
        }).onSameThread().check();
    }

    private void showPermissionsAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 101);
                })
                .setNegativeButton("CANCEL", (dialog, which) -> dialog.dismiss()).show();
    }

    private String getRealPathFromURI_API19(GetQuotation getQuotation, Uri uri) {
        String filePath = "";
        if (uri.getHost().contains("com.android.providers.media")) {
            // Image pick from recent
            String wholeID = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                wholeID = DocumentsContract.getDocumentId(uri);
            }

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } else {
            // image pick from gallery
            return  getRealPathFromURI_BelowAPI11(this,uri);
        }
    }

    private String getRealPathFromURI_BelowAPI11(GetQuotation getQuotation, Uri uri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = this.getContentResolver().query(uri, proj, null, null, null);
        int column_index
                = Objects.requireNonNull(cursor).getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private Uri getImageUri(GetQuotation requireNonNull, Bitmap imageBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), imageBitmap,"Title",null);
        return Uri.parse(path);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            // When an Image is picked
            if (requestCode == MEDIA_TYPE_IMAGE && resultCode == RESULT_OK && null != data) {
                Log.e("pick image from gallery","");
                // Get the Image from data
                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null)  {
                    Uri mImageUri=data.getData();
                    if (uriList.size()<=2){
                        uriList.add(mImageUri);
                        imagesList.add(getRealPathFromURI_API19(this,mImageUri));
                    }
                    else Snackbar.make(parent_root,"Sorry u can add 3 images only",Snackbar.LENGTH_LONG).show();
                    Log.e(" single image", String.valueOf(imagesList));
                    QuotationImageUploadAdapter imagesAdapterClass = new QuotationImageUploadAdapter(uriList, GetQuotation.this);
                    upload_img_recycle.setAdapter(imagesAdapterClass);
                    upload_img_recycle.setLayoutManager(new GridLayoutManager(this,3));
                } else {
                    if (data.getClipData() != null) {
                        Log.e("clip data",data.getClipData().toString());
                        ClipData mClipData = data.getClipData();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            if (uriList.size()<=2){
                                uriList.add(uri);
                                imagesList.add(getRealPathFromURI_API19(this,uri));
                            }
                            else Snackbar.make(parent_root,"Sorry u can add 3 images only",Snackbar.LENGTH_LONG).show();
                            Log.e("multiple imagesuri", String.valueOf(imagesList));
                            QuotationImageUploadAdapter imagesAdapterClass = new QuotationImageUploadAdapter(uriList, GetQuotation.this);
                            upload_img_recycle.setAdapter(imagesAdapterClass);
                            upload_img_recycle.setLayoutManager(new GridLayoutManager(this,3));
                        }
                    }
                }
            } else if (requestCode == PICK_CAMERA && resultCode == RESULT_OK && null != data) {
                count++;
                Log.i("CaptureImage", "onActivityResult: camera");
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                assert imageBitmap != null;
                Uri tempUri = getImageUri(Objects.requireNonNull(this), imageBitmap);
                if (uriList.size()<=2){
                    uriList.add(tempUri);
                    imagesList.add(getRealPathFromURI_API19(this,tempUri));
                }
                else Snackbar.make(parent_root,"Sorry u can add 3 images only",Snackbar.LENGTH_LONG).show();
                Log.i(String.valueOf(this), "onActivityResult: URI "+uriList.toString());
                // imagesList.add(tempUri.getPath());


                Log.e("imageslist", String.valueOf(imagesList));
                QuotationImageUploadAdapter imagesAdapterClass = new QuotationImageUploadAdapter(uriList, GetQuotation.this);
                upload_img_recycle.setAdapter(imagesAdapterClass);
                upload_img_recycle.setLayoutManager(new GridLayoutManager(this,3));


            }
            else {
                Snackbar.make(parent_root,"You haven't picked Image",Snackbar.LENGTH_LONG).show();
            }

        }catch (Exception exp){
            exp.printStackTrace();
            Snackbar.make(parent_root,"Oops! something went wrong",Snackbar.LENGTH_LONG).show();
        }
    }

    private void QuotationImagesUploadAPI() {

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[imagesList.size()];
        Log.e("url list size", String.valueOf(imagesList.size()));
        for (int index = 0; index < imagesList.size(); index++) {
            Log.d("ImagesData", "requestUploadSurvey: survey image " + index + "  " + imagesList.get(index));

            // File file = new File(uriList.get(index));
            File file = new File(imagesList.get(index));
            Log.e("file", file.toString());
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[index] = MultipartBody.Part.createFormData("files", file.getName(), surveyBody);
        }

        RequestBody customer_id = RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(preferences.getString("cust_Id", null)));
        Call<QuotationImagesUploadRes> quotationImagesUploadResCall = RestApi.get().getRestService().uploadResponseCall(surveyImagesParts,customer_id);
        quotationImagesUploadResCall.enqueue(new Callback<QuotationImagesUploadRes>() {
            @Override
            public void onResponse(Call<QuotationImagesUploadRes> call, Response<QuotationImagesUploadRes> response) {
                try {
                    if(response.body().getCode().equals("200")){
                        Snackbar.make(parent_root,"Images uploaded successfully",Snackbar.LENGTH_LONG).show();
                        InsertNewQuotationAPI(response.body().getRef_no());
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<QuotationImagesUploadRes> call, Throwable t) {

            }
        });

    }

    private void InsertNewQuotationAPI(String ref_no) {
        InsertNewQuotationReq quotationReq = new InsertNewQuotationReq();
        Desc_json desc_json = new Desc_json();
        desc_json.setDesc(Objects.requireNonNull(prod_desc_edt.getText()).toString());
        quotationReq.setCust_id(preferences.getString("cust_Id",null));
        quotationReq.setRef_no(ref_no);
        quotationReq.setLat_no("17.742475");
        quotationReq.setLong_no("83.338861");
        quotationReq.setDesc_json(desc_json);
        quotationReq.setBrand_id("5");
        quotationReq.setCat_id(preferences.getString("CatId",null));
        quotationReq.setPrd_id(preferences.getString("ProdId",null));
        quotationReq.setSub_prd_id(preferences.getString("SubProdId",null));
        quotationReq.setDistrict_id("5");
        quotationReq.setService_type(selected_item);
        quotationReq.setState_id("1");
        quotationReq.setInput_type("S");

        Log.i(TAG, "InsertNewQuotationAPI: "+quotationReq);

        Call<InsertNewQuotationRes> quotationResCall = RestApi.get().getRestService().newquotaResponseCall(quotationReq);
        quotationResCall.enqueue(new Callback<InsertNewQuotationRes>() {
            @Override
            public void onResponse(Call<InsertNewQuotationRes> call, Response<InsertNewQuotationRes> response) {
                try {
                    if (response.body().getStatus().equalsIgnoreCase("true")){

                        SuccessDialog();
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<InsertNewQuotationRes> call, Throwable t) {

            }
        });
    }

    private void SuccessDialog() {
        successDialog = new Dialog(this);
        successDialog.setCancelable(true);
        successDialog.setContentView(R.layout.success_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.alphaBlack)));
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        successDialog.getWindow().setAttributes(lp);
        TextView success_txt = successDialog.findViewById(R.id.success_text);
        AppCompatButton button = successDialog.findViewById(R.id.okay);

        success_txt.setText("Thank You \n Your quotation is placed successfully");
        button.setOnClickListener(v ->  startActivity(new Intent(GetQuotation.this,HomePage1.class)));

        successDialog.show();
    }


}
