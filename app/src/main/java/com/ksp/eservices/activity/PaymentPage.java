package com.ksp.eservices.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.customerOrderGeneration.CustomerOrderGenerationReq;
import com.ksp.eservices.model.customerOrderGeneration.CustomerOrderGenerationRes;
import com.ksp.eservices.model.getCustomerAddressByTxn.GetCustomerAddressByTxnReq;
import com.ksp.eservices.model.getCustomerAddressByTxn.GetCustomerAddressByTxnRes;
import com.ksp.eservices.network.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentPage extends BaseActivity {

    private MaterialTextView user_name, user_city, user_address;
    private SharedPreferences preferences;
    private MaterialButton change_address;
    private AppCompatButton order_generate_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Hooks
        setContentView(R.layout.activity_payment_page);
        user_name = findViewById(R.id.user_name);
        user_city = findViewById(R.id.user_adrs);
        user_address = findViewById(R.id.user_adrs1);
        change_address = findViewById(R.id.change_address);
        order_generate_btn = findViewById(R.id.order_generate_btn);

        change_address.setOnClickListener(v -> startActivity(new Intent(PaymentPage.this, AddAddress.class)));
        order_generate_btn.setOnClickListener(v -> OrderGenerateAPI());

        GetCustomerAddressByTxnAPI();
    }

    private void OrderGenerateAPI() {
        showProgress();

        CustomerOrderGenerationReq orderGenerationReq = new CustomerOrderGenerationReq();
        orderGenerationReq.setCust_id(preferences.getString("cust_Id",null));
        orderGenerationReq.setAddress_ref_no(preferences.getString("AddressTxnRefNo",null));
        orderGenerationReq.setDistrict_id("5");
        orderGenerationReq.setLat_no("17.742475");
        orderGenerationReq.setLong_no("83.338861");
        orderGenerationReq.setRef_no(preferences.getString("quot_ref_no",null));
        orderGenerationReq.setRetailer_id(preferences.getString("retailer_id",null));
        orderGenerationReq.setState_id("1");

        Call<CustomerOrderGenerationRes> generationResCall = RestApi.get().getRestService().ordgenerationResponseCall(orderGenerationReq);
        generationResCall.enqueue(new Callback<CustomerOrderGenerationRes>() {
            @Override
            public void onResponse(Call<CustomerOrderGenerationRes> call, Response<CustomerOrderGenerationRes> response) {
                try {
                    dismissDialog();
                    SuccessDialog();

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CustomerOrderGenerationRes> call, Throwable t) {
                dismissDialog();

            }
        });
    }

    private void SuccessDialog() {

        Dialog successDialog = new Dialog(this);
        successDialog.setCancelable(true);
        successDialog.setContentView(R.layout.success_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.alphaBlack)));
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        successDialog.getWindow().setAttributes(lp);
        TextView success_txt = successDialog.findViewById(R.id.success_text);
        AppCompatButton button = successDialog.findViewById(R.id.okay);

        success_txt.setText("Thank You \n Your order is placed successfully");
        button.setOnClickListener(v ->  startActivity(new Intent(PaymentPage.this,HomePage1.class)));

        successDialog.show();
    }

    private void GetCustomerAddressByTxnAPI() {

        GetCustomerAddressByTxnReq addressByTxnReq = new GetCustomerAddressByTxnReq();
        if (preferences.getString("AddressTxnRefNo",null)!=null) addressByTxnReq.setRef_no(preferences.getString("AddressTxnRefNo",null));

        Call<GetCustomerAddressByTxnRes> byTxnResCall = RestApi.get().getRestService().txn_get_address_ResponseCall(addressByTxnReq);
        byTxnResCall.enqueue(new Callback<GetCustomerAddressByTxnRes>() {
            @Override
            public void onResponse(Call<GetCustomerAddressByTxnRes> call, Response<GetCustomerAddressByTxnRes> response) {
                try {
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getADDRESS_DATA().size()>0){
                            user_name.setText(String.format("%s (%s)", response.body().getADDRESS_DATA().get(0).getName(), response.body().getADDRESS_DATA().get(0).getMobile()));
                            user_city.setText(response.body().getADDRESS_DATA().get(0).getCity());
                            user_address.setText(String.format("%s, %s, %s - %s", response.body().getADDRESS_DATA().get(0).getHouse_no(), response.body().getADDRESS_DATA().get(0).getLand_mark(), response.body().getADDRESS_DATA().get(0).getStreet(), response.body().getADDRESS_DATA().get(0).getPin_code()));
                        }
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetCustomerAddressByTxnRes> call, Throwable t) {

            }
        });
    }
}
