package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ebanx.swipebtn.SwipeButton;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.CategoryAdapter;
import com.ksp.eservices.adapter.MyOrdersAdapter;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.Categories;
import com.ksp.eservices.model.OrderItem;
import com.ksp.eservices.model.getAllCustomerAddresses.GetAllCustomerAddressesRes;
import com.ksp.eservices.model.getCustomerDashboardData.GetCustomerDashboardDataRes;
import com.ksp.eservices.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePage1 extends BaseActivity implements View.OnClickListener {

    SwipeButton swipeButton;
    RecyclerView categ_recylce, order_recycle;
    AppCompatButton quotation_but;
    CategoryAdapter categoryAdapter;
    MyOrdersAdapter myOrdersAdapter;
    List<Categories> categoriesList = new ArrayList<>();
    List<OrderItem> orderItemList = new ArrayList<>();
    TextView view_all_quot, view_all_orders;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page1);

        //SharedPreference
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Hooks
        swipeButton = findViewById(R.id.swipeBtn);
        categ_recylce = findViewById(R.id.categ_recycle);
        quotation_but = findViewById(R.id.quotation_but);
        order_recycle = findViewById(R.id.order_recycle);
        view_all_quot = findViewById(R.id.quota_view_all);
        view_all_orders = findViewById(R.id.orders_view_all);

        swipeButton.setOnStateChangeListener(active -> {
          startActivity(new Intent(HomePage1.this,CatalogueActivity.class));
        });

        GetCustomerDashboardDataAPI();

        //Adding Items to Categories
       /* categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));
        categoriesList.add(new Categories(R.drawable.lappy, "Laptop"));

        categoryAdapter = new CategoryAdapter(HomePage1.this,categoriesList);
        categ_recylce.setAdapter(categoryAdapter);
        categ_recylce.setNestedScrollingEnabled(true);
        categ_recylce.setHasFixedSize(true);
        categ_recylce.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        //Adding List to MyOrder Class
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Pending"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Success"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Pending"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Success"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Pending"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Success"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Pending"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Success"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Pending"));
        orderItemList.add(new OrderItem(R.drawable.lappy,"Laptop","Success"));

        myOrdersAdapter = new MyOrdersAdapter(HomePage1.this,orderItemList);
        order_recycle.setAdapter(myOrdersAdapter);
        order_recycle.setNestedScrollingEnabled(true);
        order_recycle.setHasFixedSize(true);
        order_recycle.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));*/

        view_all_quot.setOnClickListener(this);
        view_all_orders.setOnClickListener(this);


    }

    private void GetCustomerDashboardDataAPI() {
        showProgress();

        Call<GetCustomerDashboardDataRes> dataResCall = RestApi.get().getRestService().dashboardDataResponseCall(preferences.getString("cust_Id",null));
        dataResCall.enqueue(new Callback<GetCustomerDashboardDataRes>() {
            @Override
            public void onResponse(Call<GetCustomerDashboardDataRes> call, Response<GetCustomerDashboardDataRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getQUT_DATA().size()>0){
                            categoryAdapter = new CategoryAdapter(HomePage1.this,response.body().getQUT_DATA());
                            categ_recylce.setAdapter(categoryAdapter);
                            categ_recylce.setNestedScrollingEnabled(true);
                            categ_recylce.setHasFixedSize(true);
                            categ_recylce.setLayoutManager(new LinearLayoutManager(HomePage1.this,LinearLayoutManager.HORIZONTAL,false));
                        }
                        if (response.body().getORDER_DATA().size()>0){
                            myOrdersAdapter = new MyOrdersAdapter(HomePage1.this,response.body().getORDER_DATA());
                            order_recycle.setAdapter(myOrdersAdapter);
                            order_recycle.setNestedScrollingEnabled(true);
                            order_recycle.setHasFixedSize(true);
                            order_recycle.setLayoutManager(new LinearLayoutManager(HomePage1.this,LinearLayoutManager.HORIZONTAL,false));
                        }
                    }
                }catch (Exception exp){
                    dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<GetCustomerDashboardDataRes> call, Throwable t) {
                dismissDialog();

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == view_all_quot) startActivity(new Intent(HomePage1.this,ViewAllQuotation.class));
        else if (v == view_all_orders) startActivity(new Intent(HomePage1.this,ViewAllQuotation.class));

    }
}
