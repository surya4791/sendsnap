package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.register.RegisterReq;
import com.ksp.eservices.model.register.RegisterRes;
import com.ksp.eservices.network.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registration extends BaseActivity implements View.OnClickListener {

    public static final String TAG = Registration.class.getSimpleName();
    TextInputEditText user_name_edt, user_mobile_edt;
    RadioGroup gender;
    RadioButton radiosexbutton;
    int selectedRadioId;
    AppCompatButton reg_submit;
    RelativeLayout root;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //hooks
        setContentView(R.layout.activity_registration);
        user_name_edt = findViewById(R.id.user_name_edt);
        user_mobile_edt = findViewById(R.id.user_mobile_edt);
        gender = findViewById(R.id.gender_grp);
        reg_submit = findViewById(R.id.reg_submit);
        root = findViewById(R.id.root);
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        /*Preference mobile no get from login activity*/
        if (preferences.getString("cust_Mobile",null)!=null){
            user_mobile_edt.setText(preferences.getString("cust_Mobile", null));
        }else {user_mobile_edt.setText("");}


        //OnClick Listener
        reg_submit.setOnClickListener(this);

        //Gender Group Selection
        gender.setOnCheckedChangeListener((radioGroup, i) -> {
            selectedRadioId = gender.getCheckedRadioButtonId();
            radiosexbutton = radioGroup.findViewById(selectedRadioId);
            Log.i("GENDER", radiosexbutton.getText().toString());
        });

    }

    @Override
    public void onClick(View v) {

        if (v==reg_submit){
            validation();
        }

    }

    private void validation() {

        if (user_name_edt.getText().toString().isEmpty()){
            Snackbar.make(root,"Please enter username",Snackbar.LENGTH_LONG).show();
        }else if (user_mobile_edt.getText().toString().isEmpty()){
            Snackbar.make(root,"Please enter mobile no",Snackbar.LENGTH_LONG).show();
        }else if (gender.getCheckedRadioButtonId()==-1){
            Snackbar.make(root,"Please choose gender",Snackbar.LENGTH_LONG).show();
        }else {
            RegistrationAPI();
        }
    }

    private void RegistrationAPI() {

        showProgress();
        RegisterReq registerReq = new RegisterReq();
        registerReq.setLat_no("17.345986");
        registerReq.setLong_no("31.104569");
        registerReq.setCustomer_name(user_name_edt.getText().toString());
        registerReq.setMobile_no(user_mobile_edt.getText().toString());
        registerReq.setFulladdress("7-7-11, kondapalli vari street, Anakapalle");
        registerReq.setStateLongName("AndhraPradesh");
        registerReq.setTokenid("1234567890");

        Log.i(TAG, "RegistrationAPI: "+registerReq);

        Call<RegisterRes> registerResCall = RestApi.get().getRestService().registerresponseCall(registerReq);
        registerResCall.enqueue(new Callback<RegisterRes>() {
            @Override
            public void onResponse(Call<RegisterRes> call, Response<RegisterRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        startActivity(new Intent(Registration.this,HomePage1.class));
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RegisterRes> call, Throwable t) {
                dismissDialog();

            }
        });
    }
}
