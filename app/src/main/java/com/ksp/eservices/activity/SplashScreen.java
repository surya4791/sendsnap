package com.ksp.eservices.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;

public class SplashScreen extends AppCompatActivity {

    LottieAnimationView camera;
    MaterialTextView send, snap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
        send = findViewById(R.id.send_txt);
        snap = findViewById(R.id.snap_text);
        camera = findViewById(R.id.camera_lottie);

        camera.animate().translationX(-1600).setDuration(1000).setStartDelay(5000);
        send.animate().translationX(1400).setDuration(1000).setStartDelay(4000);
        snap.animate().translationX(1400).setDuration(1000).setStartDelay(4000);

        new Handler().postDelayed(() ->{

            startActivity(new Intent(SplashScreen.this, WelcomeScreen.class));
        },6000 );


    }
}
