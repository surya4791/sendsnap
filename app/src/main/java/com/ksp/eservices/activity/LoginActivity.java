package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.ksp.eservices.R;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.login.LoginReq;
import com.ksp.eservices.model.login.LoginRes;
import com.ksp.eservices.network.RestApi;

import eightbitlab.com.blurview.BlurView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

   // MaterialAutoCompleteTextView country;
    public static final String TAG = LoginActivity.class.getSimpleName();
    TextInputEditText mobile;
    AppCompatButton generate_otp;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Hooks
        setContentView(R.layout.activity_login_screen1);
       // country  = findViewById(R.id.country);
        mobile = findViewById(R.id.mobile);
        // parent = findViewById(R.id.login_root);
        generate_otp = findViewById(R.id.send_verify);

        // country.setText("+91 IND");

        generate_otp.setOnClickListener(v -> {
            if (mobile.getText().toString().isEmpty()){
                Snackbar.make(v,"Please enter 10 digits mobile number",Snackbar.LENGTH_LONG).show();
            }else {

                LoginAPI();
               /* if (mobile.getText().toString().equals("1234567890")){
                    startActivity(new Intent(LoginActivity.this,OTPConfirm.class));
                }else {
                    Snackbar.make(v,"Invalid mobile number",Snackbar.LENGTH_LONG).show();
                }*/
            }
        });

    }

    private void LoginAPI() {

        showProgress();
        LoginReq loginReq = new LoginReq();
        loginReq.setMobile_no(mobile.getText().toString());
        Log.i(TAG, "LoginAPI: "+loginReq);

        Call<LoginRes> loginResCall = RestApi.get().getRestService().loginresponseCall(loginReq);
        loginResCall.enqueue(new Callback<LoginRes>() {
            @Override
            public void onResponse(Call<LoginRes> call, Response<LoginRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getCode().equals("200")){
                        SharedPreferences preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);
                        preferences.edit().putString("cust_Mobile", mobile.getText().toString()).apply();
                        startActivity(new Intent(LoginActivity.this, OTPConfirm.class));
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<LoginRes> call, Throwable t) {
                dismissDialog();
            }
        });
    }
}
