package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.SUB_PRD_Catalogue_Adapter;
import com.ksp.eservices.adapter.ProductListByCatId_Adapter;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.productCategoryList.Category_master;
import com.ksp.eservices.model.productCategoryList.ProductCategoryListRes;
import com.ksp.eservices.model.productTypeDetailsById.PRODUCT_DATA;
import com.ksp.eservices.model.productTypeDetailsById.ProductTypeDetailsByIdRes;
import com.ksp.eservices.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatalogueActivity extends BaseActivity {
    public static final String TAG = CatalogueActivity.class.getSimpleName();
    RecyclerView catalogue_recycle;
    AppCompatImageView back;
    MaterialAutoCompleteTextView spin_category_list;
    ArrayList<String> arrayList_cat_id = new ArrayList<>();
    ArrayList<String> arrayList_cat_name = new ArrayList<>();
    String cat_id = "";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);

        //Shared Preference
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Hooks
        catalogue_recycle = findViewById(R.id.catalogue_list_view);
        back = findViewById(R.id.back);
        spin_category_list = findViewById(R.id.spin_category_list);

        //Product Category List API
        CatalogueApi();

        spin_category_list.setOnItemClickListener((parent, view, position, id) -> {
            cat_id = arrayList_cat_id.get(position);
            preferences.edit().putString("CatId",cat_id).apply();
            Log.i(TAG, "onCreate: "+cat_id);
            ProductListByCatIdAPI(cat_id);

        });


       /* catalogueLists.add(new CatalogueList("Women's Fashion",R.drawable.womens_fashion));
        catalogueLists.add(new CatalogueList("Men's Fashion",R.drawable.lappy1));
        catalogueLists.add(new CatalogueList("Computer",R.drawable.lappy1));
        catalogueLists.add(new CatalogueList("Mobiles",R.drawable.lappy1));
        catalogueLists.add(new CatalogueList("Smart Homes",R.drawable.lappy1));
        catalogueLists.add(new CatalogueList("Home Furniture",R.drawable.lappy1));
        catalogueLists.add(new CatalogueList("Arts & Crafts",R.drawable.lappy1));*/

        // Attach CatalogueList array to adapter class


        back.setOnClickListener(v -> startActivity(new Intent(CatalogueActivity.this,HomePage1.class)));

    }

    private void CatalogueApi() {

        Call<ProductCategoryListRes> productCategoryListResCall = RestApi.get().getRestService().productcatResponseCall();
        productCategoryListResCall.enqueue(new Callback<ProductCategoryListRes>() {
            @Override
            public void onResponse(Call<ProductCategoryListRes> call, Response<ProductCategoryListRes> response) {

                try {
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getCategory_master()!=null && response.body().getCategory_master().size()>0){

                            for (int i = 0; i<response.body().getCategory_master().size();i++){
                                arrayList_cat_name.add(response.body().getCategory_master().get(i).getCat_desc());
                                arrayList_cat_id.add(response.body().getCategory_master().get(i).getCat_id());
                            }

                            ArrayAdapter<String> Category = new ArrayAdapter<>(CatalogueActivity.this,android.R.layout.select_dialog_item,arrayList_cat_name);
                            spin_category_list.setAdapter(Category);


                        }
                    }

                }catch (Exception exp){
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ProductCategoryListRes> call, Throwable t) {

            }
        });
    }

    private void ProductListByCatIdAPI(String cat_id) {
        showProgress();
        Call<ProductTypeDetailsByIdRes> typeDetailsByIdResCall = RestApi.get().getRestService().productresponseCall(cat_id);
        typeDetailsByIdResCall.enqueue(new Callback<ProductTypeDetailsByIdRes>() {
            @Override
            public void onResponse(Call<ProductTypeDetailsByIdRes> call, Response<ProductTypeDetailsByIdRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")){

                        if (response.body().getPRODUCT_DATA()!=null && response.body().getPRODUCT_DATA().size()>0){
                            List<PRODUCT_DATA> product_dataList = response.body().getPRODUCT_DATA();
                            ProductListByCatId_Adapter catIdAdapter = new ProductListByCatId_Adapter(CatalogueActivity.this,product_dataList);
                            catalogue_recycle.setAdapter(catIdAdapter);
                            catalogue_recycle.setNestedScrollingEnabled(true);
                            catalogue_recycle.setHasFixedSize(true);
                            catalogue_recycle.setLayoutManager(new LinearLayoutManager(CatalogueActivity.this));

                        }
                    }
                }catch (Exception exp){
                    exp.printStackTrace();
                    dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<ProductTypeDetailsByIdRes> call, Throwable t) {
                dismissDialog();

            }
        });
    }
}
