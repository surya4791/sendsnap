package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.chaos.view.PinView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.otp.OTPReq;
import com.ksp.eservices.model.otp.OTPRes;
import com.ksp.eservices.network.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPConfirm extends BaseActivity {

    public static final String TAG = OTPConfirm.class.getSimpleName();
    PinView code;
    RelativeLayout parent;
    AppCompatButton otp_confirm;
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        //Hooks
        setContentView(R.layout.activity_otp_screen_1);
        code = findViewById(R.id.otp_pin);
        parent = findViewById(R.id.otp_root);
        otp_confirm = findViewById(R.id.otp_continue);
        MaterialTextView otp_mobile_txt = findViewById(R.id.otp_mobile_txt);
        MaterialTextView change_mobile = findViewById(R.id.change_mobile);

        /*Preference mobile no get from login activity*/
        if (preferences.getString("cust_Mobile",null)!=null){
            otp_mobile_txt.setText(String.format("+91 %s", preferences.getString("cust_Mobile", null)));
        }else {otp_mobile_txt.setText("");}

        otp_confirm.setOnClickListener(v -> {
            if (code.getText().toString().isEmpty()){
                Snackbar.make(v,"Please enter OTP",Snackbar.LENGTH_LONG).show();
            }else {

               OTPAPI();
            }
        });

        change_mobile.setOnClickListener(v -> startActivity(new Intent(OTPConfirm.this,LoginActivity.class)));

    }

    private void OTPAPI() {

        showProgress();
        OTPReq otpReq = new OTPReq();
        otpReq.setMobile_no(preferences.getString("cust_Mobile",""));
        otpReq.setOtp(code.getText().toString());
        Log.i(TAG, "OTPAPI: "+otpReq);

        Call<OTPRes> otpResCall = RestApi.get().getRestService().otpresponseCall(otpReq);
        otpResCall.enqueue(new Callback<OTPRes>() {
            @Override
            public void onResponse(Call<OTPRes> call, Response<OTPRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getCust_type().equalsIgnoreCase("OLD")) {
                            preferences.edit().putString("cust_Name",response.body().getCust_name()).apply();
                            preferences.edit().putString("cust_Id",response.body().getCustomer_id()).apply();
                            startActivity(new Intent(OTPConfirm.this, HomePage1.class));
                        }else if (response.body().getCust_type().equalsIgnoreCase("NEW")){
                            startActivity(new Intent(OTPConfirm.this,Registration.class));
                        }

                    }else {
                        Snackbar.make(parent,"Invalid Code",Snackbar.LENGTH_LONG).show();
                    }
                }catch (Exception ep){
                    ep.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<OTPRes> call, Throwable t) {

            }
        });
    }
}
