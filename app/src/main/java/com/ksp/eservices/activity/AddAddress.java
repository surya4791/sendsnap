package com.ksp.eservices.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.AllCustomerAddressAdapter;
import com.ksp.eservices.app.BaseActivity;
import com.ksp.eservices.model.getAllCustomerAddresses.GetAllCustomerAddressesReq;
import com.ksp.eservices.model.getAllCustomerAddresses.GetAllCustomerAddressesRes;
import com.ksp.eservices.model.insertCustomerAddress.InsertCustomerAddressReq;
import com.ksp.eservices.model.insertCustomerAddress.InsertCustomerAddressRes;
import com.ksp.eservices.network.RestApi;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddress extends BaseActivity {

    AppCompatButton confirm_btn;
    private Dialog address_dialog;
    private RecyclerView get_adrs_recycle;
    private AppCompatButton dialog_adrs_btn, del_to_this_Adrs;
    private MaterialTextView OR_adrs_txt;
    private SharedPreferences preferences;
    private TextInputEditText flat, street, landmark, city, pincode, person_name, person_email, person_mobile;
    private RadioGroup radioGroup;
    private RadioButton select_address_tag;
    private String addresstag = "";
    private String txn_ref_no = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_layout);

        //Hooks
        confirm_btn = findViewById(R.id.confirm_adress);
        flat = findViewById(R.id.add_flat_ed);
        street = findViewById(R.id.add_colony_ed);
        landmark = findViewById(R.id.add_landmark_ed);
        city = findViewById(R.id.add_city_ed);
        pincode = findViewById(R.id.add_pincode_ed);
        person_name = findViewById(R.id.add_person_ed);
        person_email = findViewById(R.id.add_email_ed);
        person_mobile = findViewById(R.id.add_mobile_ed);
        radioGroup = findViewById(R.id.address_grp);

        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        // Select Address Dialog
        SelectAddressDialog();

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            select_address_tag = findViewById(radioGroup.getCheckedRadioButtonId());
            addresstag = select_address_tag.getText().toString();
        });

        confirm_btn.setOnClickListener(v -> {
            InsertCustomerAddressApi();
        });
    }

    private void InsertCustomerAddressApi() {
        showProgress();

        InsertCustomerAddressReq addressReq = new InsertCustomerAddressReq();
        addressReq.setHouse_no(Objects.requireNonNull(flat.getText()).toString());
        addressReq.setStreet(Objects.requireNonNull(street.getText()).toString());
        addressReq.setLand_mark(Objects.requireNonNull(landmark.getText()).toString());
        addressReq.setCity(Objects.requireNonNull(city.getText()).toString());
        addressReq.setPin_code(Objects.requireNonNull(pincode.getText()).toString());
        addressReq.setAddress_type(addresstag);
        addressReq.setName(Objects.requireNonNull(person_name.getText()).toString());
        addressReq.setEmail(Objects.requireNonNull(person_email.getText()).toString());
        addressReq.setMobile(Objects.requireNonNull(person_mobile.getText()).toString());
        addressReq.setCust_id(preferences.getString("cust_Id", null));
        addressReq.setDistrict_id("1");
        addressReq.setState_id("1");
        addressReq.setCountry_id("1");

        Call<InsertCustomerAddressRes> insertCustomerAddressResCall = RestApi.get().getRestService().insertaddressResponseCall(addressReq);
        insertCustomerAddressResCall.enqueue(new Callback<InsertCustomerAddressRes>() {
            @Override
            public void onResponse(Call<InsertCustomerAddressRes> call, Response<InsertCustomerAddressRes> response) {
                try {
                    dismissDialog();
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        preferences.edit().putString("AddressTxnRefNo", response.body().getTxn_ref_no()).apply();
                        startActivity(new Intent(AddAddress.this, PaymentPage.class));
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<InsertCustomerAddressRes> call, Throwable t) {
                dismissDialog();
            }
        });
    }

    private void SelectAddressDialog() {

        address_dialog = new Dialog(AddAddress.this);
        address_dialog.setCancelable(false);
        address_dialog.setContentView(R.layout.get_all_customer_addresses);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        address_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.alphaBlack)));
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        address_dialog.getWindow().setAttributes(lp);
        AppCompatImageView close_dialog = address_dialog.findViewById(R.id.img_close);
        get_adrs_recycle = address_dialog.findViewById(R.id.get_adrs_recylce);
        dialog_adrs_btn = address_dialog.findViewById(R.id.dialog_adrs_btn);
        del_to_this_Adrs = address_dialog.findViewById(R.id.delv_to_this_adrs);
        OR_adrs_txt = address_dialog.findViewById(R.id.OR_adrs_txt);

        GetAddressApi();

        del_to_this_Adrs.setOnClickListener(v -> {
            startActivity(new Intent(AddAddress.this, PaymentPage.class));
        });

        dialog_adrs_btn.setOnClickListener(v -> {
            address_dialog.dismiss();
        });

        close_dialog.setOnClickListener(v -> {
            address_dialog.dismiss();
        });
        address_dialog.show();
    }

    private void GetAddressApi() {
        GetAllCustomerAddressesReq addressesReq = new GetAllCustomerAddressesReq();
        addressesReq.setCustomer_id(preferences.getString("cust_Id", null));

        Call<GetAllCustomerAddressesRes> getAllCustomerAddressesResCall = RestApi.get().getRestService().getallCustomerResponseCall(addressesReq);
        getAllCustomerAddressesResCall.enqueue(new Callback<GetAllCustomerAddressesRes>() {
            @Override
            public void onResponse(Call<GetAllCustomerAddressesRes> call, Response<GetAllCustomerAddressesRes> response) {
                try {
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        if (response.body().getADDRESS_DATA().size() > 0) {
                            AllCustomerAddressAdapter addressAdapter = new AllCustomerAddressAdapter(AddAddress.this, response.body().getADDRESS_DATA());
                            get_adrs_recycle.setAdapter(addressAdapter);
                            get_adrs_recycle.setNestedScrollingEnabled(true);
                            get_adrs_recycle.setHasFixedSize(true);
                            get_adrs_recycle.setLayoutManager(new LinearLayoutManager(AddAddress.this));
                        }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetAllCustomerAddressesRes> call, Throwable t) {

            }
        });
    }
}
