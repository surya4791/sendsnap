package com.ksp.eservices.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.ksp.eservices.MainActivity;
import com.ksp.eservices.R;
import com.ksp.eservices.adapter.RetalierListAdapter;
import com.ksp.eservices.model.getAllRetailerForQuat.GetAllRetailerForQuatRes;
import com.ksp.eservices.model.getAllRetailerForQuat.RETAILER_DATA;
import com.ksp.eservices.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceOrder extends AppCompatActivity {
    AppCompatImageView prod_img;
    AppCompatTextView prod_name, retailers, best_price;
    RecyclerView retailers_recycle;
    List<RETAILER_DATA> retailer_dataList = new ArrayList<>();
    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences("SendSnap", Context.MODE_PRIVATE);

        setContentView(R.layout.place_order);
        prod_img = findViewById(R.id.prod_img);
        prod_name = findViewById(R.id.product_name);
        retailers = findViewById(R.id.retailers);
        best_price = findViewById(R.id.best_price);
        retailers_recycle = findViewById(R.id.retails_recycle);

        //GetAllRetailersForQuat API
        GetAllRetailersForQuat();

    }

    private void GetAllRetailersForQuat() {
        Call<GetAllRetailerForQuatRes> getAllRetailerForQuatResCall = RestApi.get().getRestService().getallretailersResponseCall(getIntent().getStringExtra("Quota_Ref"));
        getAllRetailerForQuatResCall.enqueue(new Callback<GetAllRetailerForQuatRes>() {
            @Override
            public void onResponse(Call<GetAllRetailerForQuatRes> call, Response<GetAllRetailerForQuatRes> response) {
                try {
                    if (response.body().getStatus().equalsIgnoreCase("true")){
                        if (response.body().getQUT_DATA().size()>0 && response.body().getQUT_DATA()!=null){
                            prod_name.setText(response.body().getQUT_DATA().get(0).getPrd_name());
                            retailers.setText(String.format("Retailers Available - %s", response.body().getQUT_DATA().get(0).getTotal_retailer_cnt()));
                            best_price.setText(String.format("Best Price - %s", response.body().getQUT_DATA().get(0).getBest_price()));
                            Glide.with(PlaceOrder.this).load(response.body().getQUT_DATA().get(0).getFilepath()).into(prod_img);
                            preferences.edit().putString("quot_ref_no",response.body().getQUT_DATA().get(0).getRef_no()).apply();
                        }
                        if (response.body().getRETAILER_DATA().size()>0 && response.body().getRETAILER_DATA()!=null){
                            retailer_dataList = response.body().getRETAILER_DATA();
                            RetalierListAdapter listAdapter = new RetalierListAdapter(PlaceOrder.this,retailer_dataList);
                            retailers_recycle.setAdapter(listAdapter);
                            retailers_recycle.setHasFixedSize(true);
                            retailers_recycle.setLayoutManager(new LinearLayoutManager(PlaceOrder.this));
                            retailers_recycle.setNestedScrollingEnabled(true);
                        }
                    }

                }catch (Exception exp){
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetAllRetailerForQuatRes> call, Throwable t) {

            }
        });
    }

}
