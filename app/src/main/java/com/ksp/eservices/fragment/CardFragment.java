package com.ksp.eservices.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.ksp.eservices.Interface.CardInterface;
import com.ksp.eservices.R;

public class CardFragment extends Fragment {
    private CardView mCardView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.welcome_card, container, false);
        mCardView = view.findViewById(R.id.cardview);
        mCardView.setMaxCardElevation(mCardView.getCardElevation()
                * CardInterface.MAX_ELEVATION_FACTOR);
        return view;
    }

    public CardView getCardView() {
        return mCardView;
    }
}
