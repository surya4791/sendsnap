package com.ksp.eservices.Interface;

import androidx.cardview.widget.CardView;

public interface CardInterface {
    int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
