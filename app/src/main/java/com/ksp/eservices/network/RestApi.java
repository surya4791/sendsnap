package com.ksp.eservices.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ksp.eservices.model.customerOrderGeneration.CustomerOrderGenerationReq;
import com.ksp.eservices.model.customerOrderGeneration.CustomerOrderGenerationRes;
import com.ksp.eservices.model.getAllCustomerAddresses.GetAllCustomerAddressesReq;
import com.ksp.eservices.model.getAllCustomerAddresses.GetAllCustomerAddressesRes;
import com.ksp.eservices.model.getAllCustomerQutations.GetAllCustomerQutationsRes;
import com.ksp.eservices.model.getAllRetailerForQuat.GetAllRetailerForQuatRes;
import com.ksp.eservices.model.getAllRetailerForQuat.quotation_upload_image.QuotationImagesUploadRes;
import com.ksp.eservices.model.getCustomerAddressByTxn.GetCustomerAddressByTxnReq;
import com.ksp.eservices.model.getCustomerAddressByTxn.GetCustomerAddressByTxnRes;
import com.ksp.eservices.model.getCustomerDashboardData.GetCustomerDashboardDataRes;
import com.ksp.eservices.model.insertCustomerAddress.InsertCustomerAddressReq;
import com.ksp.eservices.model.insertCustomerAddress.InsertCustomerAddressRes;
import com.ksp.eservices.model.insertNewQuotation.InsertNewQuotationReq;
import com.ksp.eservices.model.insertNewQuotation.InsertNewQuotationRes;
import com.ksp.eservices.model.login.LoginReq;
import com.ksp.eservices.model.login.LoginRes;
import com.ksp.eservices.model.otp.OTPReq;
import com.ksp.eservices.model.otp.OTPRes;
import com.ksp.eservices.model.productCategoryList.ProductCategoryListRes;
import com.ksp.eservices.model.productTypeDetailsById.ProductTypeDetailsByIdRes;
import com.ksp.eservices.model.register.RegisterReq;
import com.ksp.eservices.model.register.RegisterRes;
import com.ksp.eservices.model.subProductCategoryDetailsById.SubProductCategoryDetailsByIdRes;
import com.ksp.eservices.utils.Connections;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class RestApi {

    public static String BASE_URL = Connections.BASE_URL;
    private static SharedPreferences sp;

    private static RestApi instance = null;

    public static synchronized RestApi get() {
        if (instance == null) {
            instance = new RestApi();
        }
        return instance;
    }

    public static void init(Context context) {
        sp = context.getSharedPreferences("SendSnap", Context.MODE_PRIVATE);
    }

    public RestService getRestService() {
        if (sp == null) {
            throw new IllegalStateException("init method should be called first.");
        }
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        return buildAdapter(BASE_URL, RestService.class, builder);
    }

    private <T> T buildAdapter(String baseUrl, Class<T> clazz, OkHttpClient.Builder builder) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //
        OkHttpClient defaultHttpClient = builder
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Auth-Key", "ordmangRestApi");
                    /*.header("Client-Type",  "APP");*/
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                })
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(defaultHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(clazz);
    }

    public interface RestService {

        //Login
        @POST("auth/customerMobilelogin")
        Call<LoginRes> loginresponseCall(@Body LoginReq loginReq);

        //OTP
        @POST("auth/checkLoginOtp")
        Call<OTPRes> otpresponseCall(@Body OTPReq otpReq);

        //Registration
        @POST("auth/registerNewCustomer")
        Call<RegisterRes> registerresponseCall(@Body RegisterReq registerReq);

        //Product Category List
        @POST("productmaster/getProductCategoryList")
        Call<ProductCategoryListRes> productcatResponseCall();

        //Product List By Category Id
        @POST("productmaster/{catId}/getProductTypeDetailsById")
        Call<ProductTypeDetailsByIdRes> productresponseCall(@Path("catId") String catId);

        //SubProductCategoryDetailsById
        @POST("productmaster/{sub_prd_id}/getSubProductCategoryDetailsById")
        Call<SubProductCategoryDetailsByIdRes> sub_prd_response_Call(@Path("sub_prd_id") String sub_prd_id);

        //getAllRetailerForQuat
        @GET("customer/{txn_ref_no}/getAllRetailerForQuat")
        Call<GetAllRetailerForQuatRes> getallretailersResponseCall(@Path("txn_ref_no") String txn_ref_no);

        //QuotationImagesUpload
        @Multipart
        @POST("upload/images-upload")
        Call<QuotationImagesUploadRes> uploadResponseCall(
                @Part MultipartBody.Part[] files,
                @Part("customer_id") RequestBody customer_id
                );

        //InsertNewQuotation
        @POST("customer/insertNewQuotation")
        Call<InsertNewQuotationRes> newquotaResponseCall (@Body InsertNewQuotationReq insertNewQuotationReq);

        //getAllCustomerAddresses
        @POST("customer/getAllCustomerAddresses")
        Call<GetAllCustomerAddressesRes> getallCustomerResponseCall (@Body GetAllCustomerAddressesReq getAllCustomerAddressesReq);

        // insertCustomerAddress
        @POST("customer/insertCustomerAddress")
        Call<InsertCustomerAddressRes> insertaddressResponseCall (@Body InsertCustomerAddressReq customerAddressReq);

        // getCustomerAddressByTxn
        @POST("customer/getCustomerAddressByTxn")
        Call<GetCustomerAddressByTxnRes> txn_get_address_ResponseCall (@Body GetCustomerAddressByTxnReq addressByTxnReq);

        // getCustomerDashboardData
        @POST("customer/{cust_Id}/getCustomerDashboardData")
        Call<GetCustomerDashboardDataRes> dashboardDataResponseCall (@Path("cust_Id") String cust_id);

        //getAllCustomerQuotations
        @POST("customer/{cust_Id}/getAllCustomerQutations")
        Call<GetAllCustomerQutationsRes> custquotationResponseCall (@Path("cust_Id") String cust_id);

        //custmoerOrderGeneration
        @POST("productmaster/customerOrderGeneration")
        Call<CustomerOrderGenerationRes> ordgenerationResponseCall (@Body CustomerOrderGenerationReq generationReq);
    }
}
