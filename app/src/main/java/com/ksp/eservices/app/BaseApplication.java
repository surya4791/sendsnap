package com.ksp.eservices.app;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.ksp.eservices.network.RestApi;

public class BaseApplication extends MultiDexApplication {
    private static BaseApplication mInstance;
    // private static MySMSBroadCastReceiver mySMSBroadCastReceiver;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);

    }

    // TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/your_font_file.ttf");

    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }
    @Override
    public void onCreate() {

        super.onCreate();
        mInstance = this;
        //FirebaseApp.initializeApp(this);
        //Initialise rest api
        RestApi.init(this);



     /*   CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/montserrat_regular.ttf")
                .setFontAttrId(R.font.montserrat_regular)
                .build()
        );*/
    }
}

