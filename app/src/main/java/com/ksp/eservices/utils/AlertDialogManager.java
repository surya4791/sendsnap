package com.ksp.eservices.utils;

import android.content.Context;
import android.content.DialogInterface;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ksp.eservices.R;

public class AlertDialogManager {

    public void showAlertDialog(Context context, String title, String message, Boolean status){

        MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(context);
        // Setting Dialog Title
        dialogBuilder.setTitle(title);

        // Setting Dialog Message
        dialogBuilder.setMessage(message);

        dialogBuilder.setCancelable(false);

        if(status != null)
            // Setting alert dialog icon
            dialogBuilder.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        dialogBuilder.setPositiveButton("OK", (dialog, which) -> {

        }).setNegativeButton("Cancel",(dialog, which) -> {

        });

        // Showing Alert Message
        dialogBuilder.show();
        dialogBuilder.create();


    }

}
